<?php
/*
 * Copy rights info:
 * Owner: NetSuite Inc. 
 * Copyright (c) 2008, NetSuite Inc. 
 * All rights reserved.
*/

require_once 'directory_v2008.2.php';

global $myDirectory;
global $endpoint;

$version = "2008_2_r1";

class nsComplexObject {
					
	var $nsComplexObject_type;
	var $nsComplexObject_namespace;
	var $nsComplexObject_fields;	
	
	function __construct ($type, array $fields = null) {

		$this->nsComplexObject_type = $type;
		$this->nsComplexObject_namespace = getNameSpace($this->nsComplexObject_type);		
		if (!is_null($fields))
			$this->setFields($fields);
				
	}
	
	function setFields(array $fieldArray=null) {
		
		if ($fieldArray == null)
			return;
				
		global $myDirectory;
			
		foreach ($fieldArray as $fldName => $fldValue) {	
			
			if (((is_null($fldValue) || $fldValue == "") && $fldValue !== false) || arrayValuesAreEmpty($fldValue))
				continue;
			
			getFieldType($this->nsComplexObject_type . "/" . $fldName);
			
			if ($fldValue === 'false') {
				
				$this->nsComplexObject_fields[$fldName] = FALSE;
				
			} elseif ( $fldValue instanceof nsComplexObject ) {
				
				$this->nsComplexObject_fields[$fldName] = $fldValue;				
				
			} elseif (isset($myDirectory[$this->nsComplexObject_type . '/' . $fldName]) && is_array($fldValue) && array_is_associative($fldValue)) {
				
				$obj = new nsComplexObject(getFieldType($this->nsComplexObject_type . '/' . $fldName));
				$obj->setFields($fldValue);
				$this->nsComplexObject_fields[$fldName] = $obj;
				
			} elseif (isset($myDirectory[$this->nsComplexObject_type . '/' . $fldName]) && is_array($fldValue) && !array_is_associative($fldValue)) {

				foreach ($fldValue as $object) {
					
					if ($object instanceof nsComplexObject) {
						
						$val[] = $object;
							
					} elseif (getFieldType($this->nsComplexObject_type . '/' . $fldName) == "string") {
						
						$val[] = $object;
						
					} else {
					
						$obj = new nsComplexObject(getFieldType($this->nsComplexObject_type . '/' . $fldName));	
						$obj->setFields($object);
						$val[] = $obj;
						
					}
					
				}
								
				$this->nsComplexObject_fields[$fldName] = $val;
				
			} else {
				
				$this->nsComplexObject_fields[$fldName] = $fldValue;
				
			}
										
		}			
			
	}

	function getSoapVar() {
		
		if ( !isset($this->nsComplexObject_fields) || $this->nsComplexObject_fields == null ) {
			
			return null;
			
		}
		
		foreach ( $this->nsComplexObject_fields as $fldName => $fldVal ) {
			
			if ( $fldVal instanceof nsComplexObject ) {

				$this->nsComplexObject_fields[$fldName] = $fldVal->getSoapVar();
				
			} elseif ( is_array($fldVal) ) {
				
				foreach ($fldVal as $object) {
					
					if ($object instanceof nsComplexObject) {
						
						$val[] = $object->getSoapVar();
							
					} elseif (getFieldType($this->nsComplexObject_type . '/' . $fldName) == "string") {
						
						$val[] = $object;
						
					} else {
						
						$obj = new nsComplexObject(getFieldType($this->nsComplexObject_type . '/' . $fldName));	
						$obj->setFields($object);
						$val[] = $obj->getSoapVar();
						
					}
					
				}
				
				$this->nsComplexObject_fields[$fldName] = $val;
				
			}
			
		}
		
		return new SoapVar($this->nsComplexObject_fields, SOAP_ENC_OBJECT, $this->nsComplexObject_type, $this->nsComplexObject_namespace);		
		
	}
	
	function getFields() {
		
		return $this->nsComplexObject_fields;
		
	}
	
	function getField($fieldName) {
		
		if (isset($this->nsComplexObject_fields[$fieldName]))
			return $this->nsComplexObject_fields[$fieldName];
		
	}
	
	function clearField( $fieldName ) {
		
		if (isset($this->nsComplexObject_fields[$fieldName]))
			unset($this->nsComplexObject_fields[$fieldName]);
		
	}
	
}


class nsRecordRef extends nsComplexObject { 
	
	function __construct (array $fields = null) {
		
		parent::__construct('RecordRef');
		parent::setFields($fields);		
		
	}
			
}


class nsListOrRecordRef extends nsComplexObject {
	
	function __construct (array $fields) {
		
		parent::__construct('ListOrRecordRef');
		parent::setFields($fields);		
		
	}	
		
}


class nsCustomRecordRef extends nsComplexObject { 

	function __construct (array $fields = null) {

	parent::__construct('CustomRecordRef');
	parent::setFields($fields); 
	
	}
}

class nsSessionResponse {
	
	var $isSuccess;
	var $statusDetail;
	var $wsRoleList;	
	
	function __construct ($sessionResponse) {
		
		$this->isSuccess = $sessionResponse->status->isSuccess;
		
		if ( isset($sessionResponse->status->statusDetail) ) {
			
			$this->statusDetail = getStatusDetail($sessionResponse->status->statusDetail);
			
		}
		
		if (is_array($sessionResponse->wsRoleList->wsRole)) {
			
			foreach ($sessionResponse->wsRoleList->wsRole as $wsRole) {
			
				$wsRoleNSObject = new nsComplexObject('WsRole');
				
				$wsRoleFields = array();
				$wsRoleFields['isDefault'] = $wsRole->isDefault;
				$wsRoleFields['isInactive'] = $wsRole->isInactive;
				$wsRoleFields['role'] = new nsRecordRef(array(	'internalId' 	=> $wsRole->role->internalId,
																'name'			=> $wsRole->role->name)
														);
														
				$wsRoleNSObject->setFields($wsRoleFields);
				
				$this->wsRoleList[] = $wsRoleNSObject;

			}
			
		} elseif (count($sessionResponse->wsRoleList->wsRole) > 0) {
			
			$wsRoleNSObject = new nsComplexObject('WsRole');
			
			$wsRoleFields = array();
			$wsRoleFields['isDefault'] = $sessionResponse->wsRoleList->wsRole->isDefault;
			$wsRoleFields['isInactive'] = $sessionResponse->wsRoleList->wsRole->isInactive;
			$wsRoleFields['role'] = new nsRecordRef(array(	'internalId' 	=> $sessionResponse->wsRoleList->wsRole->role->internalId,
															'name'			=> $sessionResponse->wsRoleList->wsRole->role->name)
													);
			
			$wsRoleNSObject->setFields($wsRoleFields);
				
			$this->wsRoleList[] = $wsRoleNSObject;
						
		}
		
	}
	
}


class nsWriteResponse {
	
	var $isSuccess;
	var $statusDetail;
	var $recordRef;	
	
	function __construct ($soapResponse) {
		
		$this->isSuccess = $soapResponse->status->isSuccess;
		
		if ( isset($soapResponse->status->statusDetail) ) {
			
			$this->statusDetail = getStatusDetail($soapResponse->status->statusDetail);
			
		}
		
		if ($this->isSuccess) {			
						
			$this->recordRef = new nsRecordRef(array(	'type' 			=> $soapResponse->baseRef->type, 
														'internalId' 	=> $soapResponse->baseRef->internalId,
														'externalId' 	=> $soapResponse->baseRef->externalId	)
												);
		}
		
	}
	
}


class nsStatusDetail {
	
	var $code;
	var $message;
	var $type;
	
	function __construct ($cd, $mes, $tp) {
		
		$this->code = $cd;
		$this->message = $mes;
		$this->type = $tp;
		
	}
	
}


class nsReadResponse {
	
	var $isSuccess;
	var $statusDetail;
	var $record;	
	
	function __construct ($soapResponse) {
		
		$this->isSuccess = $soapResponse->status->isSuccess;		
		
		if ( isset($soapResponse->status->statusDetail) ) {
			
			$this->statusDetail = getStatusDetail($soapResponse->status->statusDetail);
			
		}
		
		$cleanRecord = new nsComplexObject($soapResponse->record->nsComplexObject_type);
		$cleanRecord->nsComplexObject_fields = $soapResponse->record->nsComplexObject_fields;
		
		$this->record = $cleanRecord;
		
	}
	
}


class nsSearchResponse { 
	
	var $isSuccess;
	var $statusDetail;
	var $totalRecords;
	var $pageSize;
	var $totalPages;
	var $pageIndex;
	var $searchId;
	var $recordList;	
	
	function __construct ($searchResponse) {
		
		$this->isSuccess = $searchResponse->status->isSuccess;
		
		if ( isset($searchResponse->status->statusDetail) ) {
			
			$this->statusDetail = getStatusDetail($searchResponse->status->statusDetail);
			
		}
		
		$this->totalRecords = $searchResponse->totalRecords;
		$this->pageSize = $searchResponse->pageSize;
		$this->totalPages = $searchResponse->totalPages;
		$this->pageIndex = $searchResponse->pageIndex;
		$this->searchId = $searchResponse->searchId;
		
		if (isset($searchResponse->recordList) )
		{
			if (is_array($searchResponse->recordList->record)) {
				
				foreach ($searchResponse->recordList->record as $rec) {
	
					$cleanRecord = new nsComplexObject($rec->nsComplexObject_type);
					$cleanRecord->nsComplexObject_fields = $rec->nsComplexObject_fields;
					
					$this->recordList[] = $cleanRecord;
	
				}
				
			} elseif (count($searchResponse->recordList->record) > 0) {
				
				$cleanRecord = new nsComplexObject($searchResponse->recordList->record->nsComplexObject_type);
				$cleanRecord->nsComplexObject_fields = $searchResponse->recordList->record->nsComplexObject_fields;
				
				$this->recordList[] = $cleanRecord;
			}
		}
			
		else if (isset($searchResponse->searchRowList) )
		{
			if (is_array($searchResponse->searchRowList->searchRow)) {
				
				foreach ($searchResponse->searchRowList->searchRow as $rec) {
	
					//$cleanRecord = new nsComplexObject($rec->nsComplexObject_type);
					//$cleanRecord->nsComplexObject_fields = $rec->nsComplexObject_fields;
					
					//$this->recordList[] = $cleanRecord;
					$this->recordList[] = $rec;
	
				}
				
			} elseif (count($searchResponse->searchRowList->searchRow) > 0) {
				
				//$cleanRecord = new nsComplexObject($searchResponse->searchRowList->searchRow->nsComplexObject_type);
				//$cleanRecord->nsComplexObject_fields = $searchResponse->recordList->searchRow->nsComplexObject_fields;
				
				//$this->recordList[] = $cleanRecord;
				$this->recordList[] = $searchResponse->searchRowList->searchRow;
			}
		}	
			
	}
	
}


class nsGetRecordResult {
	
	var $isSuccess;
	var $statusDetail;
	var $totalRecords;
	var $recordList;
	
	function __construct($response) {
		
		$this->isSuccess = $response->status->isSuccess;
		$this->totalRecords = $response->totalRecords;
		
		if ( isset($response->status->statusDetail) ) {
			
			$this->statusDetail = getStatusDetail($response->status->statusDetail);
			
		}
		
		if (is_array($response->recordList->record)) {
			
			foreach ($response->recordList->record as $rec) {
			
				$cleanRecord = new nsComplexObject($rec->nsComplexObject_type);
				$cleanRecord->nsComplexObject_fields = $rec->nsComplexObject_fields;
				
				$this->recordList[] = $cleanRecord;

			}
			
		} elseif (count($response->recordList->record) > 0) {
			
			$cleanRecord = new nsComplexObject($response->recordList->record->nsComplexObject_type);
			$cleanRecord->nsComplexObject_fields = $response->recordList->record->nsComplexObject_fields;
			
			$this->recordList[] = $cleanRecord;
						
		}
		
	}
	
}


class nsGetRecordRefResult {
	
	var $isSuccess;
	var $statusDetail;
	var $totalRecords;
	var $recordRefList;
	
	function __construct($response) {
		
		$this->isSuccess = $response->status->isSuccess;
		$this->totalRecords = $response->totalRecords;
		
		if ( isset($response->status->statusDetail) ) {
			
			$this->statusDetail = getStatusDetail($response->status->statusDetail);
			
		}
		
		if (is_array($response->recordRefList->recordRef)) {
			
			foreach ($response->recordRefList->recordRef as $recRef) {
			
				$recRefFields = array();
				$recRefFields['internalId'] = $recRef->internalId;
				$recRefFields['name'] = $recRef->name;
				
				$this->recordRefList[] = new nsRecordRef($recRefFields);

			}
			
		} elseif (count($response->recordRefList->record) > 0) {
			
			$recRefFields = array();
			$recRefFields['internalId'] = $response->recordRefList->recordRef->internalId;
			$recRefFields['name'] = $response->recordRefList->recordRef->name;
			
			$this->recordRefList[] = new nsRecordRef($recRefFields);
						
		}
		
	}
	
}


class nsGetItemAvailabilityResult {
	
	var $isSuccess;
	var $statusDetail;
	var $itemAvailabilityList;
	
	function __construct ( $getItemAvailabilityResult ) {
		
		$this->isSuccess = $getItemAvailabilityResult->status->isSuccess;
		
		if ( isset($searchResponse->status->statusDetail) ) {
			
			$this->statusDetail = getStatusDetail($searchResponse->status->statusDetail);
			
		}
		
		$records = $getItemAvailabilityResult->itemAvailabilityList->itemAvailability;
		
		if (is_array($records)) {
			
			foreach ($records as $rec) {

				$this->itemAvailabilityList[] = $rec;

			}
			
		} elseif (count($records) > 0) {
			
			$this->itemAvailabilityList[] = $records;
			
		}
		
	}
	
	function getTotalQuantityOnHand ( $itemId ) {
		
		$totalQOH = 0;
		
		foreach( $this->itemAvailabilityList as $itemAvail ) {
			
			if ($itemAvail->getField('item')->getField('internalId') == $itemId) {
			
				$totalQOH = $totalQOH + $itemAvail->getField('quantityOnHand');
				
			}
			
		}
		
		return $totalQOH;
		
	}
	
	function getTotalQuantityAvailable ( $itemId ) {
		
		$totalQAvailable = 0;
		
		foreach( $this->itemAvailabilityList as $itemAvail ) {
			
			if ($itemAvail->getField('item')->getField('internalId') == $itemId) {
			
				$totalQAvailable = $totalQAvailable + $itemAvail->getField('quantityAvailable');
				
			}
			
		}
		
		return $totalQAvailable;
		
	}
	
	function getItemName ( $itemId ) {
		
		$itemName = "";
		
		foreach( $this->itemAvailabilityList as $itemAvail ) {
			
			if ($itemAvail->getField('item')->getField('internalId') == $itemId) {
			
				$itemName = $itemAvail->getField('item')->getField('name');
				break;
				
			}
			
		}
		
		return $itemName;
		
	}
	
}


class nsItemAvailability extends nsComplexObject { 
	
	function __construct (array $fields = null) {
		
		parent::__construct('ItemAvailability');
		parent::setFields($fields);		
		
	}
	
}


class nsPricingMatrix extends nsComplexObject { 
	
	function __construct (array $fields = null) {
		
		parent::__construct('PricingMatrix');
		parent::setFields($fields);		
		
	}
	
	function setFields (array $fields) {
		
		if ( isset($fields["pricing"]) && count($fields["pricing"]) == 1) {
					
			$pricingArray = array();
			
			foreach ( $fields as $fldName => $fldValue ) {
				
				if ($fldName == "pricing") {
					
					$pricingArray[] = $fldValue;
					
				}
				
			}
			
			$fields["pricing"] = $pricingArray;
			
		}
		
		parent::setFields($fields);
		
	}
	
	function getPriceValue ( $priceLevel, $quantity, $currency ) {
		
		$pricing = $this->getPricing( $priceLevel, $currency );
		
		$priceArray = $pricing->getField("priceList")->getField("price");
				
		foreach ( $priceArray as $price ) {
			
			if ( $price->getField('quantity') == $quantity )
				
				return $price->getField('value');
			
		}
		
		return "";	
		
	}
	
	function getPricesFromPriceLevel ( $priceLevel, $currency, $range=false ) {
		
		$pricing = $this->getPricing( $priceLevel, $currency );
		
		$priceArray = $pricing->getField("priceList")->getField("price");
		
		$p = array();
				
		for ( $i = 0; $i < count($priceArray); $i++ ) {

			$thisPrice = $priceArray[$i];
			$quantity = (int)$thisPrice->getField('quantity');
			$value = $thisPrice->getField('value');
			
			if ($range) {
				
				$k = $i + 1;
				
				$key = "";
								
				if ( $quantity == 0 ) {
					
					$quantity = 1;
					
				}
				if ($k == count($priceArray)) {
					
					$key = " and up";
					
				} else {
					
					$key = " to " . ((int)$priceArray[$k]->getField('quantity') - 1);
					
				}
				$p["$quantity" .  "$key"] = $value;
				
			} else {
			
				$p[$quantity] = $value;
				
			}
		
		}
		
		return $p;		
		
	}
	
	private function getPricing ( $priceLevel, $currency ) {
		
		$pricingArray = $this->getField("pricing");
		
		foreach ( $pricingArray as $pricing ) {
			
			if ( $pricing->getField('currency')->getField('internalId') == $currency && $pricing->getField('priceLevel')->getField('internalId') == $priceLevel ) {
				
				return $pricing;
			
			}
			
		}
		
		throw new Exception ("Pricing object was not found. PriceLevel = $priceLevel, Currency = $currency");
		
	}
	
}


class nsPriceList extends nsComplexObject { 
	
	function __construct (array $fields = null) {
		
		parent::__construct('PriceList');
		parent::setFields($fields);		
		
	}
	
	function setFields (array $fields) {
		
		if ( isset($fields["price"]) && count($fields["price"]) == 1) {
					
			$priceArray = array();
			
			foreach ( $fields as $fldName => $fldValue ) {
				
				if ($fldName == "price") {
					
					$priceArray[] = $fldValue;
					
				}
				
			}
			
			$fields["price"] = $priceArray;
			
		}
		
		parent::setFields($fields);
		
	}
	
}


class nsAddressbookList extends nsComplexObject { 
	
	function __construct ($type, array $fields = null) {
		
		parent::__construct($type);
		parent::setFields($fields);		
		
	}
	
	function setFields (array $fields) {
		
		if ( isset($fields["addressbook"]) && count($fields["addressbook"]) == 1) {
					
			$addressbookArray = array();
			
			foreach ( $fields as $fldName => $fldValue ) {
				
				if ($fldName == "addressbook") {
					
					$addressbookArray[] = $fldValue;
					
				}
				
			}
			
			$fields["addressbook"] = $addressbookArray;
			
		}
		
		parent::setFields($fields);
		
	}

	function getBillingAddress() {
		
		$address_array = $this->getField('addressbook');
		
		foreach ( $address_array as $address ) {
			
			if ( $address->getField('defaultBilling') == "true" ) {
				
				return $address;
				
			}
			
		}
		
	}
	
	function getShippingAddress() {
		
		$address_array = $this->getField('addressbook');
		
		foreach ( $address_array as $address ) {
			
			if ( $address->getField('defaultShipping') == "true" ) {
				
				return $address;
				
			}
			
		}
		
	}
	
}


abstract class nsHost {
	
	const live = "https://webservices.netsuite.com";
	const beta = "https://webservices.beta.netsuite.com";
	const sandbox = "https://webservices.sandbox.netsuite.com";
	
}


class nsClient {
	
	private $client;		
	private $soapHeaders = null;
			
	function __construct ( $host=nsHost::live ) {

		global $endpoint;
		global $version;
		
		$typemap = array(
		
			array(	"type_name" => 'Record',
					"type_ns" 	=> getNameSpace('Record'),
					"from_xml" 	=> 'deserializeRecord'
			),
			array(	"type_name" => 'ItemAvailability',
					"type_ns" 	=> getNameSpace('ItemAvailability'),
					"from_xml" 	=> 'deserializeItemAvailability'
			)
			
		);

		function deserializeRecord ($obj) {
			
			$obj = cleanUpNamespaces($obj);
		
			$xml = simplexml_load_string($obj, 'SimpleXMLElement', LIBXML_NOCDATA);

			$x = deserializeSimpleXML($xml);
		
			return $x;
			
		}
		
		function deserializeItemAvailability ($obj) {
			
			$obj = cleanUpNamespaces($obj);
		
			$xml = simplexml_load_string($obj, 'SimpleXMLElement', LIBXML_NOCDATA);

			$x = deserializeSimpleXML($xml, "ItemAvailability");
		
			return $x;
			
		}
						
		function cleanUpNamespaces($xml_root) {
		
			$xml_root = str_replace('xsi:type', 'xsitype', $xml_root);
		
			$record_element = new SimpleXMLElement($xml_root);
			
			foreach ($record_element->getDocNamespaces() as $name => $ns) {
				
				if ($name != "")
					$xml_root = str_replace($name . ':', '', $xml_root);	
				
			}
			
			$record_element = new SimpleXMLElement($xml_root);			
						
			foreach($record_element->children() as $field) {
				
				$field_element = new SimpleXMLElement($field->asXML());
				
				foreach ($field_element->getDocNamespaces() as $name2 => $ns2) {
					
					if ($name2 != "") 
						$xml_root = str_replace($name2 . ':', '', $xml_root);
																
				}								
				
			}	
			
			return $xml_root;
			
		}
								
		
		$this->client = new SoapClient($host . "/wsdl/v" . $endpoint . "_0/netsuite.wsdl",
										array(	"location" 				=> $host . "/services/NetSuitePort_" . $endpoint,
												"trace" 				=> 1, 																																		
												"connection_timeout" 	=> 5, 
												"typemap" 				=> $typemap,
												"user_agent" 			=> "PHP-SOAP/" . phpversion() . " + NetSuite PHP Toolkit " . $version)
										);
		 
	}
		
	function login($email, $password, $account, $role=null) {
		
		$loginParams = array();
		$loginParams['email'] = $email;
		$loginParams['password'] = $password;
		$loginParams['account'] = $account;
		if ( !($role instanceof nsComplexObject) ) {
		
			$roleRecRef = new nsRecordRef(array ('internalId' => $role));
			$loginParams['role'] = $roleRecRef->getSoapVar();
			
		} else {
				
			$loginParams['role'] = $role;
			
		}
		
		$loginResponse = $this->makeCall("login", array(array('passport' => $loginParams)));
		
		return new nsSessionResponse($loginResponse->sessionResponse);
		
	}
	
	function ssoLogin($partnerId, $authenticationToken) {
		
		$ssoLoginParams = array();
		$ssoLoginParams['partnerId'] = $partnerId;
		$ssoLoginParams['authenticationToken'] = $authenticationToken;
		
		$ssoLoginResponse = $this->makeCall("ssoLogin", array(array('ssoPassport' => $ssoLoginParams)));
		
		return new nsSessionResponse($ssoLoginResponse->sessionResponse);
		
	}
	
	function logout() {
		
		$this->clearPassport();
		
		$logoutResponse = $this->makeCall("logout");
		
		return new nsSessionResponse($logoutResponse->sessionResponse);
		
	}
	
	function changePasswordOrEmail($currentPassword, $newEmail, $newEmail2, $newPassword=null, $newPassword2=null, $justThisAccount=null) {
		
		$changePasswordOrEmailCredentials = array();
		$changePasswordOrEmailCredentials['currentPassword'] = $currentPassword;
		$changePasswordOrEmailCredentials['newEmail'] = $newEmail;
		$changePasswordOrEmailCredentials['newEmail2'] = $newEmail2;
		$changePasswordOrEmailCredentials['newPassword'] = $newPassword;
		$changePasswordOrEmailCredentials['newPassword2'] = $newPassword2;
		$changePasswordOrEmailCredentials['justThisAccount'] = $justThisAccount;
		
		$changePasswordOrEmailResponse = $this->makeCall("changePasswordOrEmail", array(array('changePasswordOrEmailCredentials' => $changePasswordOrEmailCredentials)));
		
		return new nsSessionResponse($changePasswordOrEmailResponse->sessionResponse);
		
	}
	
	function add(nsComplexObject $record) {
		
		$addResponse = $this->makeCall("add", array(array('record' => $record->getSoapVar())));		
		
		return new nsWriteResponse($addResponse->writeResponse);
		
	}
	
	function addList(array $records) {
		
		foreach ($records as $recs) {
			
			$addListRecords[] = $recs->getSoapVar();
			
		}
		
		$addResponseList = $this->makeCall("addList", array('record' => $addListRecords));		
		
		foreach ($addResponseList->writeResponseList->writeResponse as $addResponse) {
			
			$writeResponseArray[] = new nsWriteResponse($addResponse);	
			
		}
		
		return $writeResponseArray;
		
	}
	
	function update(nsComplexObject $record) {
		
		$updateResponse = $this->makeCall("update", array(array('record' => $record->getSoapVar())));
		
		return new nsWriteResponse($updateResponse->writeResponse);
		
	}
	
	function updateList(array $records) {
		
		foreach ($records as $recs) {
			
			$updateListRecords[] = $recs->getSoapVar();
			
		}
		
		$updateResponseList = $this->makeCall("updateList", array('record' => $updateListRecords));
		
		foreach ($updateResponseList->writeResponseList->writeResponse as $updateResponse) {
			
			$writeResponseArray[] = new nsWriteResponse($updateResponse);	
			
		}
		
		return $writeResponseArray;
		
	}
	
	function get(nsRecordRef $recordRef) {
		
		$getResponse = $this->makeCall("get", array(array('baseRef' => $recordRef->getSoapVar())));	
				
		return new nsReadResponse ($getResponse->readResponse);			
		
	}
	
	function getList(array $recordRefs) {
		
		foreach ($recordRefs as $recs) {
			
			$recsToGet[] = $recs->getSoapVar();
			
		}
		
		$getListResponse = $this->makeCall("getList", array(array('baseRef' => $recsToGet)));				
		
		if ( count($getListResponse->readResponseList->readResponse) == 1 ) {
			
			$getListResponseArray[] = new nsReadResponse( $getListResponse->readResponseList->readResponse );
			
		} else {
			
			for ($i = 0; $i < count($getListResponse->readResponseList->readResponse); $i++) {
				
				$getListResponseArray[] = new nsReadResponse( $getListResponse->readResponseList->readResponse[$i] );	
				
			}
			
		}
		
		return $getListResponseArray;			
		
	}
	
	function getAll($recordType) {
		
		$getAllResponse = $this->makeCall("getAll", array(array('record' => array('recordType' => $recordType))));
		
		return new nsGetRecordResult( $getAllResponse->getAllResult );			
		
	}
	
	function delete(nsComplexObject $baseRef) {
		
		$deleteResponse = $this->makeCall("delete", array(array('baseRef' => $baseRef->getSoapVar())));	
				
		return new nsWriteResponse($deleteResponse->writeResponse);
		
	}
	
	function deleteList(array $baseRefs) {
		
		foreach ($baseRefs as $baseRef) {
			
			$deleteListRecords[] = $baseRef->getSoapVar();
			
		}
		
		$deleteResponseList = $this->makeCall("deleteList", array('baseRef' => $deleteListRecords));		
		
		foreach ($deleteResponseList->writeResponseList->writeResponse as $deleteResponse) {
			
			$writeResponseArray[] = new nsWriteResponse($deleteResponse);	
			
		}
		
		return $writeResponseArray;
		
	}
	
	function search(nsComplexObject $searchRecord) {
		
		$searchResponse = $this->makeCall("search", array(array('searchRecord' => $searchRecord->getSoapVar())));
			
		return new nsSearchResponse ($searchResponse->searchResult);			
		
	}
	
	function searchMore($pageIndex) {
		
		$searchResponse = $this->makeCall("searchMore", array(array('pageIndex' => $pageIndex)));
			
		return new nsSearchResponse ($searchResponse->searchResult);			
		
	}
	
	function searchMoreWithId($searchId, $pageIndex) {
		
		$searchResponse = $this->makeCall("searchMoreWithId", array(array('pageIndex' => $pageIndex, 'searchId' => $searchId)));
			
		return new nsSearchResponse ($searchResponse->searchResult);			
		
	}
	
	function searchNext() {
		
		$searchResponse = $this->makeCall("searchNext");
			
		return new nsSearchResponse ($searchResponse->searchResult);			
		
	}
	
	function getSavedSearch($searchType) {
		
		$getSavedSearchResponse = $this->makeCall("getSavedSearch", array(array('record' => array('searchType' => $searchType))));
		
		return new nsGetRecordRefResult( $getSavedSearchResponse->getSavedSearchResult );			
		
	}
	
	function initialize(nsComplexObject $initializeRecord) {
		
		$getResponse = $this->makeCall("initialize", array(array('initializeRecord' => $initializeRecord->getSoapVar())));	
				
		return new nsReadResponse ($getResponse->readResponse);			
		
	}
	
	function initializeList(array $initializeRecords) {
		
		foreach ($initializeRecords as $recs) {
			
			$recsToInitialize[] = $recs->getSoapVar();
			
		}
		
		$initializeListResponse = $this->makeCall("initializeList", array(array('initializeRecord' => $recsToInitialize)));				
		
		if ( count($initializeListResponse->readResponseList->readResponse) == 1 ) {
			
			$initializeListResponseArray[] = new nsReadResponse( $initializeListResponse->readResponseList->readResponse );
			
		} else {
			
			for ($i = 0; $i < count($initializeListResponse->readResponseList->readResponse); $i++) {
				
				$initializeListResponseArray[] = new nsReadResponse( $initializeListResponse->readResponseList->readResponse[$i] );	
				
			}
			
		}
		
		return $initializeListResponseArray;			
		
	}
	
	function getCustomization($getCustomizationType) {
		
		$getCustomizationResponse = $this->makeCall("getCustomization", array(array('customizationType' => array('getCustomizationType' => $getCustomizationType))));
		
		return new nsGetRecordResult( $getCustomizationResponse->getCustomizationResult );			
		
	}
	
	function getSelectValue( $fieldType, $searchCriteria=null ) {
		
		$getSVFields = array();
		$getSVFields['fieldType'] = $fieldType;
		$getSVFields['searchCriteria'] = $searchCriteria;
		
		$getSelectValueField = new nsComplexObject("GetSelectValueField", $getSVFields);
		
		$getSelectValueResponse = $this->makeCall("getSelectValue", array(array('fieldName' => $getSelectValueField->getSoapVar())));
		
		return new nsGetRecordRefResult( $getSelectValueResponse->getSelectValueResult );			
		
	}
	
	function getItemAvailability($itemAvailabilityFilter) {
		
		$getItemAvailabilityResult = $this->makeCall("getItemAvailability", array(array('itemAvailabilityFilter' => $itemAvailabilityFilter->getSoapVar())));
		
		return new nsGetItemAvailabilityResult($getItemAvailabilityResult->getItemAvailabilityResult);			
		
	}
	
	function attach( nsComplexObject $attachReference ) {
		
		$attachResponse = $this->makeCall("attach", array(array('attachReferece' => $attachReference)));
		
		return new nsWriteResponse($attachResponse->writeResponse);
	
	}
	
	function detach( nsComplexObject $detachReference ) {
		
		$detachResponse = $this->makeCall("detach", array(array('detachReference' => $detachReference)));
		
		return new nsWriteResponse($detachResponse->writeResponse);		
		
	}
	
	function getDeleted( nsComplexObject $getDeletedFilter ) {
		
		$getDeletedResult = $this->makeCall("getDeleted", array(array('getDeletedFilter' => $getDeletedFilter->getSoapVar())));
		
		return $getDeletedResult;
		
	}
	
	private function makeCall ($function_name, $arguments=array()) {
		
		$headers = $this->getRequestHeaders();
		
		$response = $this->client->__soapCall(	$function_name, 
											$arguments, 
											NULL, 
											$headers
											);	
		
		if ( file_exists(dirname(__FILE__) . '/nslog') ) {
		
			$req = dirname(__FILE__) . '/nslog' . "/" . date("Ymd.His") . "." . milliseconds() . "-" . $function_name . "-request.xml";
			$Handle = fopen($req, 'w');
			$Data = $this->client->__getLastRequest();
			
			$Data = cleanUpNamespaces($Data);				
		
			$xml = simplexml_load_string($Data, 'SimpleXMLElement', LIBXML_NOCDATA);
		
			$passwordFields = &$xml->xpath("//password | //password2 | //currentPassword | //newPassword | //newPassword2 | //ccNumber | //ccSecurityCode | //socialSecurityNumber");
		
			foreach ($passwordFields as &$pwdField) {
				
				(string)$pwdField[0] = "[Content Removed for Security Reasons]";
			
			}

			$stringCustomFields = &$xml->xpath("//customField[@xsitype='StringCustomFieldRef']");
			
			foreach ($stringCustomFields as $field) {
			
				(string)$field->value = "[Content Removed for Security Reasons]";
			
			}
						
			$xml_string = str_replace('xsitype', 'xsi:type', $xml->asXML());
			
			fwrite($Handle, $xml_string);
			fclose($Handle); 	
			
			$resp = dirname(__FILE__) . '/nslog' . "/" . date("Ymd.His") . "." . milliseconds() . "-" . $function_name . "-response.xml";
			$Handle = fopen($resp, 'w');
			$Data = $this->client->__getLastResponse();
			fwrite($Handle, $Data);
			fclose($Handle);

		}
											
		return $response;
											
	}
	
	function setSearchPreferences ($bodyFieldsOnly = true, $pageSize = 100, $retSrchCols = false) {
		
		$this->soapHeaders["searchPreferences"] = new SoapHeader(	getNameSpace("SearchPreferences"), 
																	"searchPreferences", 
																	new SoapVar(	array(	"bodyFieldsOnly" => $bodyFieldsOnly, 
																							"pageSize" => $pageSize	,
																							"returnSearchColumns" => $retSrchCols 
																					), 
																					SOAP_ENC_OBJECT
																	),
																	false, 
																	"http://schemas.xmlsoap.org/soap/actor/next" 
													);
		
	}
	
	function clearSearchPreferences () {
		
		unset($this->soapHeaders["searchPreferences"]);
		
	}
	
	function setPreferences ($ignoreReadOnlyFields=null, $warningAsError=null, $disableMandatoryCustomFieldValidation=null, $disableSystemNotesForCustomFields=null, $useConditionalDefaultsOnAdd=null, $useConditionalDefaultsOnUpdate=null) {
		
		$prefs = array();
		
		if (!is_null($ignoreReadOnlyFields))
			$prefs["ignoreReadOnlyFields"] = $ignoreReadOnlyFields;
		if (!is_null($warningAsError))
			$prefs["warningAsError"] = $warningAsError;
		if (!is_null($disableMandatoryCustomFieldValidation))
			$prefs["disableMandatoryCustomFieldValidation"] = $disableMandatoryCustomFieldValidation;
		if (!is_null($disableSystemNotesForCustomFields))
			$prefs["disableSystemNotesForCustomFields"] = $disableSystemNotesForCustomFields;
		if (!is_null($useConditionalDefaultsOnAdd))
			$prefs["useConditionalDefaultsOnAdd"] = $useConditionalDefaultsOnAdd;
		if (!is_null($useConditionalDefaultsOnUpdate))
			$prefs["useConditionalDefaultsOnUpdate"] = $useConditionalDefaultsOnUpdate;
									

		$this->soapHeaders["preferences"] = new SoapHeader(	getNameSpace("Preferences"), 
															"preferences", 
															new SoapVar($prefs, 
																		SOAP_ENC_OBJECT
															),
															false, 
															"http://schemas.xmlsoap.org/soap/actor/next" 
											);
		
	}
	
	function clearPreferences () {
		
		unset($this->soapHeaders["preferences"]);
		
	}
	
	function setApplicationInfo($applicationId) {
		
		$this->soapHeaders["applicationInfo"] = new SoapHeader(	getNameSpace("ApplicationInfo"),
																"applicationInfo",
																$applicationId,
																false, 
																"http://schemas.xmlsoap.org/soap/actor/next" 																		
												);
		
	}
	
	function clearApplicationInfo () {
		
		unset($this->soapHeaders["applicationInfo"]);
		
	}
	
	function setSessionInfo($userId) {
		
		$this->soapHeaders["sessionInfo"] = new SoapHeader(	getNameSpace("SessionInfo"),
															"sessionInfo",
															$userId,
															false, 
															"http://schemas.xmlsoap.org/soap/actor/next" 																		
											);
		
	}
	
	function clearSessionInfo () {
		
		unset($this->soapHeaders["sessionInfo"]);
		
	}
	
	function setPartnerInfo($partnerId) {
		
		$this->soapHeaders["partnerInfo"] = new SoapHeader(	getNameSpace("PartnerInfo"),
															"partnerInfo",
															$partnerId,
															false, 
															"http://schemas.xmlsoap.org/soap/actor/next" 																		
												);
		
	}
	
	function clearPartnerInfo () {
		
		unset($this->soapHeaders["partnerInfo"]);
		
	}
	
	function setDocumentInfo($nsId) {				
		
		$this->soapHeaders["documentInfo"] = new SoapHeader(getNameSpace("DocumentInfo"),
															"documentInfo",
															$nsId,
															false, 
															"http://schemas.xmlsoap.org/soap/actor/next"																		
												);
		
	}
	
	function clearDocumentInfo () {
		
		unset($this->soapHeaders["documentInfo"]);
		
	}
	
	function setPassport($email, $password, $account, $role) {
		
		$passport = array();
		$passport['email'] = $email;
		$passport['password'] = $password;
		$passport['account'] = $account;
		$role = new nsRecordRef(array('internalId' => $role));
		$passport['role'] = $role->getSoapVar();
		
		$this->soapHeaders["passport"] = new SoapHeader(getNameSpace("Passport"),
														"passport",
														new SoapVar($passport,
																	SOAP_ENC_OBJECT
														),
														false, 
														"http://schemas.xmlsoap.org/soap/actor/next"																		
												);
		
	}
	
	function clearPassport () {
		
		unset($this->soapHeaders["passport"]);
		
	}
	
	private function getRequestHeaders() {

		if (count($this->soapHeaders) == 0) {
		
			return null;
			
		} 
		
		$headers = array();
		
		foreach ($this->soapHeaders as $header) {
			
			$headers[] = $header;
			
		}
		
		if (count($headers) == 1) {
			
			return $headers[0];
			
		} else {
			
			return $headers;
			
		}
		
	}
	
}


########### DESERIALIZER ##############

function deserializeSimpleXML (SimpleXMLElement $record_element, $parent="") {
		
	if ( $parent == "" ) {
		
		foreach($record_element->attributes() as $attributeName => $attributeValue)
		{
			if ($attributeName == 'xsitype') {
				
				$parent = (string) $attributeValue;
				
			} else {
				
				continue;			
				
			}
		}
		
	}
	
	if		( $parent == "ItemAvailability" )		$record = new nsItemAvailability();
	elseif 	( $parent == "PricingMatrix")			$record = new nsPricingMatrix();
	else	$record = new nsComplexObject($parent);
			
	foreach($record_element->attributes() as $attributeName => $attributeValue)
	{
		
		if ($attributeName == 'xsitype') {
		
			continue;
			
		} else {
			
			$record->setFields(array($attributeName => (string)$attributeValue));			
			
		}
		
	}
	
	foreach ($record_element->children() as $fieldName => $fieldValue)
	{				
		if ($fieldValue->children()) { 
			
			if 		( getFieldType($parent . "/" . $fieldName) == "PricingMatrix")	$nsField = new nsPricingMatrix();				
			elseif 	( getFieldType($parent . "/" . $fieldName) == "PriceList")		$nsField = new nsPriceList();
			elseif 	( getFieldType($parent . "/" . $fieldName) == $parent . "AddressbookList")		$nsField = new nsAddressbookList($parent . "AddressbookList");					
			else	$nsField = new nsComplexObject(getFieldType($parent . "/" . $fieldName));
				
			foreach ($fieldValue as $fieldValue_name => $fieldValue_value) {
				
				if (count($fieldValue->$fieldValue_name) == 1) { 

					
					if ($fieldValue_value->children()) {

						$nsField->setFields(array($fieldValue_name => deserializeSimpleXML($fieldValue_value, !is_null(getTypeFromXML($fieldValue_value)) ? getTypeFromXML($fieldValue_value) : getFieldType($nsField->nsComplexObject_type . "/" . $fieldValue_name))));
						
					} else {							
						
						$nsField->setFields(array($fieldValue_name => (string)$fieldValue_value));
						
					}
					
				} else { 
					
					if ($fieldValue_value->children()) {
						
						$u[] = deserializeSimpleXML($fieldValue_value, !is_null(getTypeFromXML($fieldValue_value)) ? getTypeFromXML($fieldValue_value) : getFieldType($nsField->nsComplexObject_type . "/" . $fieldValue_name));											
						
					} else {		

						$u[] = (string)$fieldValue_value;						
						
					}	
										
				}
									
			}
			
			if (isset($u)) {
				
				$nsField->setFields(array($fieldValue_name => $u));
				unset($u); 
				
			}
			
			foreach ($fieldValue->attributes() as $z => $d) {
				
				$nsField->setFields(array($z => (string)$d));	
				
			}
						
			if (count($record_element->$fieldName) == 1) {
				
				unset($t);
				$record->setFields(array($fieldName => $nsField));
				
			} else {
				
				$t[] = $nsField;
				$record->setFields(array($fieldName => $t));
				
			}
			
		} else {
			
			if (count($record_element->$fieldName) == 1) {
			
				unset($j); 
				$record->setFields(array($fieldName => (string)$fieldValue));	

			} else {
				
				$j[] = (string)$fieldValue;
				$record->setFields(array($fieldName => $j));
				
			}
			
		}
			
			
	}
	
	return $record;
}


########### DIRECTORY FUNCTIONS ##############

function getNameSpace($complexTypeName) {
	
	global $myDirectory;
	
	if (strpos($complexTypeName, '/') === TRUE) {
		
		throw new Exception('ComplexTypeName cannot have "/"');
		
	}

	$namespace = $myDirectory[$complexTypeName];
		
	if ($namespace != null) {
		
		return $namespace;
		
	} else {
		
		foreach ($myDirectory as $key => $value) {
			
			if (strtolower($key) == strtolower($complexTypeName)) {
				
				return $value;			
				
			}
			
		}
		
	}
		
	throw new Exception ('ComplexType reference was not found in directory. ComplexTypeName = ' . $complexTypeName);	
	
}


function getFieldType ($fieldPath) {

	global $myDirectory;
	
	if (strpos($fieldPath, '/') === FALSE) {
		
		throw new Exception('Missing "/". Value passed should be <objectName>/<fieldName>');
		
	}
		
	list($parent, $field) = explode("/", $fieldPath);
	
	$compTypeName = $myDirectory[$parent . '/' . $field];

	if ($compTypeName == null) {
		
		throw new Exception ("<ComplexType/field> was not found in directory. Argument = $fieldPath");
		
	} else {
		
		return $compTypeName;
		
	}
	
}


########### UTIL FUNCTIONS ##############

function array_is_associative ($array)
{
    if ( is_array($array) && ! empty($array) )
    {
        for ( $iterator = count($array) - 1; $iterator; $iterator-- )
        {
            if ( ! array_key_exists($iterator, $array) ) { return true; }
        }
        return ! array_key_exists(0, $array);
    }
    return false;
}


function arrayValuesAreEmpty ($array)
{
	if (!is_array($array))
		return false;
			
	foreach ($array as $key => $value) {
		
		if ( $value === false || ( !is_null($value) && $value != "" && !arrayValuesAreEmpty($value))) {
			
			return false;
			
		}
	}
	
	return true;
	
}


function milliseconds()
{
    $m = explode(' ',microtime());
    return (int)round($m[0]*10000,4);
} 


function getStatusDetail ($sDetail) {
					
	if ( count($sDetail) == 1 ) {
					
		$statusDetail[] = new nsStatusDetail($sDetail->code, $sDetail->message, $sDetail->type);
		
	} else {
	
		foreach ($sDetail as $statDetail) {
			
			$statusDetail[] = new nsStatusDetail($statDetail->code, $statDetail->message, $statDetail->type);
			
		}
	
	}

	return $statusDetail;
				
}

function getTypeFromXML(SimpleXMLElement $extract_from) {
			
	foreach($extract_from->attributes() as $a => $b)
	{
		if ($a == 'xsitype') {
			
			return (string)$b;
			
		} 
		
	}
	
	return NULL;
	
}

?>
