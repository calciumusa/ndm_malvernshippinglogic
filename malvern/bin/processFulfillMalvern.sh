#!/bin/sh
###############################################################################
# Program:    processFulfillMalvern.sh
# Author:     Venkat Sankaran, DSG
# Date:       11/19/2007
# Desc:       Shell script to extract Shipping records for Malvern
#
# Usage:
#               processFulfillMalvern.sh
#
# Date          Who             Version         Desc
# ===========   ========        =========       ================================
#
#
###############################################################################


# Check if the script is already running.
if [ $TERM = "dumb" ]; then
        . $HOME/bin/masterInit.sh
else
        . ./masterInit.sh
fi


MAIL_TO=`cat $INTERFACE_DIR/mail_list.dat`
ERR_MAIL_TO=`cat $INTERFACE_DIR/fulfill_mail_list.dat`
pCount=`ps -ef|egrep "mdMalvern2NSCreateFulfillment.php"|grep -v grep|wc -l`

echo " PHP count= ${pCount} "

while [ $pCount -gt 0 ]
do
        echo "Netsuite integration already running..."
	sleep 20
	pCount=`ps -ef|egrep "mdMalvern2NSCreateFulfillment.php"|grep -v grep|wc -l`

#        echo "Netsuite integration already running. We will try again soon .." | mail -s "Error running On Demand Malvern Fulfillment Integration script" $MAIL_TO 
        echo "Netsuite integration already running. We will try again soon .." | mutt -s "Error running On Demand Malvern Fulfillment Integration script" $MAIL_TO 
        #exit 1
done

#mail -s "Mantels Fulfillment Extract-On Demand Request-Started" $ERR_MAIL_TO

echo "processFulfilltMalvern.sh On Demand started at " `date`
echo ""

SYSDATE=`date "+%m%d%y%H%M %m/%d/%y %H:%M:%S"`
SYSDATE=`echo $SYSDATE | cut -c-10`

DATA_DIR=$INTERFACE_DIR/incoming
ARCHIVE_DIR=$INTERFACE_DIR/archive

MAIL_TO=`cat $INTERFACE_DIR/mail_list.dat`
# MAIL_TO=`cat $INTERFACE_DIR/mail_raju.dat`
CONFIG_FILE=$INTERFACE_DIR/login_params.xml

cd $INTERFACE_DIR 

find $DATA_DIR -type f -name export*.txt -print |while read DATA_FILE
do
	STATUS=0
	LOGFILE=$INTERFACE_DIR/log/processMalvernFulfill$SYSDATE.log
	echo "Processing file Name : $DATA_FILE "
	php $INTERFACE_DIR/php/mdMalvern2NSCreateFulfillment.php $CONFIG_FILE $LOGFILE $DATA_FILE
	mv $DATA_FILE $ARCHIVE_DIR

	echo "Checking for error in the log file ..."
	ERR=`cat $LOGFILE|grep -c  -i "Error"`
	if [ $ERR != 0 ]; then
		STATUS=-1
		echo "Errors found in the log file..."
#		mail -s "Error running processMalvernFulfill script" $ERR_MAIL_TO <$LOGFILE
		mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -s "Error running processMalvernFulfill script" -- $ERR_MAIL_TO < $LOGFILE		
	fi

	echo "Checking for exception in the log file ..."
	EXP=`cat $LOGFILE|grep -c -i "Exception"`
	if [ $EXP != 0 ]; then
		STATUS=-1
		echo "Exception found in the log file..."
#		mail -s "Exception found when running processMalvernFulfill script" $ERR_MAIL_TO <$LOGFILE
		mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -s "Exception found when running processMalvernFulfill script" -- $ERR_MAIL_TO < $LOGFILE		
	
	fi
	if [ $STATUS != -1 ]; then
#		mail -s "Completed when running processMalvernFulfill script" $ERR_MAIL_TO <$LOGFILE
              mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -s "Completed when running processMalvernFulfill script" -- $ERR_MAIL_TO < $LOGFILE

	fi

done

echo ""
echo ""
echo "processFulfilltMalvern.sh On Demand finished at " `date`

