#!/bin/sh

echo "Malvern Fulfillment Integrtion started from new server" | mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -s "Malvern Fulfillment Integration started from new server" -- rramaswamy@demandsolutionsgroup.com

INTERFACE_DIR=/var/www/html/mantels/
cd $INTERFACE_DIR/bin
SYSDATE=`date "+%m%d%y%H%M %m/%d/%y %H:%M:%S"`
SYSDATE=`echo $SYSDATE | cut -c-10`
LOG=$INTERFACE_DIR/log/processMalvernFulfillMain$SYSDATE.log
$INTERFACE_DIR/bin/processFulfillMalvern.sh >$LOG


