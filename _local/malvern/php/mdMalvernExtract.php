<?php
require_once('common/PHPtoolkit.php');
require_once('common/class_Util.php');
function startProcess($configFile, $logFileName, $dataDir)
{
	global $myNSclient,$inputLogFile, $integratedWith;

	try
	{
		if ($configFile == '')
			$configFile = "login_params.xml";

		Util::init($configFile, $logFileName);

		$myNSclient = new nsClient( nsHost::live );

		// set request level credentials. (email, password, account#, internal id of role)
		$loginResp = $myNSclient->login(Util::$loginSXML->NSuserName, Util::$loginSXML->NSpassword, Util::$loginSXML->NSaccount, Util::$loginSXML->NSroleKey);
		if (!$loginResp->isSuccess)
		{
			throw new Exception("Unable to Login to NetSuite");
		}
		$myNSclient->setSearchPreferences(false,1000,true);

		getCustomList();
		processMalvernExtract($dataDir);
	}

	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return false;
	}
}

function getCustomList()
{
	global $myNSclient, $customListResp;
	$customListResp = $myNSclient->getCustomization('customList');
}


function processMalvernExtract($dataDir)
{
	try
	{
		global $myNSclient,$totRecs,$errRecs,$succRecs, $integratedWith, $shipmentId, $companyName, $shippingToUSA, $soTranId;

		Util::logMessage("Starting Malvern extract at " . date("c"));
		Util::logMessage("");

		$transSrchRowBasic = new nsComplexObject("TransactionSearchRowBasic");
		$transSrchRowInternalId = new nsComplexObject("SearchColumnSelectField");
		$transSrchRowInternalId->setFields(array("customLabel" => "InternalId"));
		$transSrchRowBasic->setFields(array("internalId" => $transSrchRowInternalId));

		$custSrchRowBasic = new nsComplexObject("CustomerSearchRowBasic");
		$companyNameSrchRow = new nsComplexObject("SearchColumnStringField");
		$companyNameSrchRow->setFields(array("customLabel" => "CompanyName") );
		$custSrchRowBasic->setFields(array("companyName" => $companyNameSrchRow));

		$transSrchRow = new nsComplexObject("TransactionSearchRow");
		$transSrchRow->setFields(array("basic" => $transSrchRowBasic, "customerJoin" => $custSrchRowBasic));
		$salesOrderSearch = new nsComplexObject("TransactionSearchAdvanced");		
		$salesOrderSearch->setFields(array("savedSearchId" => "633", "columns" => $transSrchRow));

		// Invoke search operation
		$searchResponse = $myNSclient->search($salesOrderSearch);
		if (!$searchResponse->isSuccess)
		{
			throw new Exception("Unsuccessful Response");
		}
		Util::logMessage("Records returned : " . $searchResponse->totalRecords);

		if ($searchResponse->totalRecords == 0)
		{
			Util::logMessage("No Records to Process");
			return;
		}

		if (!isset($dataDir)) {
			$dataDir = ".";
		}

		$datFile = $dataDir . '/NetSuite2Malvern.txt';
		$errorFile = $dataDir . '/NetSuite2MalvernErrors.txt';
		
		$integratedWith = 'EW';
		$datHandle = fopen($datFile, 'w');
		$datHandle2 = fopen($errorFile, 'w');
		
		$totRecs = 0;
		$errRecs = 0;
		$succRecs = 0;
		$shipmentId = 0;
		foreach ($searchResponse->recordList as $rec)
		{
			$totRecs++;
			$soInternalId = $rec->basic->internalId->searchValue->internalId;
			$companyName = $rec->customerJoin->companyName->searchValue;
			$shippingToUSA = true;
			$soTranId = "";
			if (extractMalvernSalesOrder($soInternalId, $datHandle))
			{
				//updateProcessed($soInternalId);

				updateProcessedMalvern($soInternalId, $soTranId, $datHandle2);
				if ($shippingToUSA == false)
				{
					updateProcessed($soInternalId);
				}
				$succRecs++;
			}
			else
			{
				$errRecs++;
			}
		}
		Util::logMessage("");
		Util::logMessage("Total Records Processed : " . $totRecs);
		Util::logMessage("Records processed successfully : " . $succRecs);
		Util::logMessage("Records failed : " . $errRecs);

		Util::logMessage("");
		Util::logMessage("Malvern extract completed at " . date("c"));

		fclose($datHandle);
		fclose($datHandle2);
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return false;
	}
}

function getSORec($intId)
{
	try
	{
		global $myNSclient;
		$recRef = new nsRecordRef(array( "internalId" => $intId, "type" => "salesOrder"));
		$getResp = $myNSclient->get($recRef);
		return $getResp->record;
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return false;
	}

}

function extractMalvernSalesOrder($soInternalId, $datHandle)
{
	try
	{
		global $lineCount, $palletized, $quantity, $commodityLinesArray, $lineItemRate, $lineItemAmount, $lineId, $shipmentId, $companyName, $shipMethodId, $shippingToUSA, $soTranId;

		Util::logMessage("Processing Sales Order Id : " . $soInternalId);
		//BOL Header Variables
		$soRec = getSORec($soInternalId);
		$recFields = $soRec->nsComplexObject_fields;
		$itemList = $soRec->nsComplexObject_fields["itemList"]->nsComplexObject_fields;
		$custFields = $recFields["customFieldList"];
		$custFieldList = $custFields->nsComplexObject_fields["customField"];
	
		if (checkMultiLineProcessed($custFieldList))
		{
			return true;
		}
	
		if (checkBackOrder($itemList))
		{
			Util::logError("There are back order items..");
			return false;
		}
	
		$recFields = $soRec->nsComplexObject_fields;
		$deptId = $recFields["department"]->nsComplexObject_fields["internalId"];
		$deptName = $recFields["department"]->nsComplexObject_fields["name"];
		
		if ($deptId == 0 || $deptId == null || $deptId == '')
		{
			Util::logError("Department is blank..");
			return false;
		}
	
		if ( strpos($deptName," : ") > 1)
		{
			$deptName = substr($deptName, strpos($deptName," : ") + 3);
		}

		if ($companyName == null)
		{
			$companyName = '';
		}
		
		$payerName1 = $deptName;
	
		$BOLHeader;
		$commodityLinesArray = array();
		$recType = '1 ';
		$BLNumber = '';
		$BLDate = date("m-d-Y");
		$terms	= 'PP';
		$pickupDate = date("m-d-Y");
		$SCACcode = '';
		$proNumber = '';
	
		$shipToID = '';
		$shipToName1 = '';
//		$shipToName2 = $companyName;
		$shipToName2 = '';
		$shipToStore = '';
		$shipToDept = '';
		$shipToAddress1 = '';
		$shipToAddress2 = '';
		$shipToCity = '';
		$shipToState = '';
		$shipToZIP = '';
		$shipToCountry = '';
		$shipToContact = '';
		$shipToPhone = '';
		
		$shipMethodId = '';
		$departmentId = '';
		$signatureRequired = '';
		$liftGate = '';
		$shipMemo = '';
		$customerPhone = '';
		$commentLinePhoneNo = '';
		$freightQuote = '';
		$serviceType = '';
		$altPhone = '';		
		foreach ($recFields  as  $fieldName => $fieldValue)
		{
			if ( $fieldValue instanceof nsComplexObject )
			{
				if ($fieldValue->nsComplexObject_type == 'RecordRef')
				{
					if ($fieldName == 'shipMethod')
					{
						$shipMethodId = 	$fieldValue->nsComplexObject_fields['internalId'];
					}
					else if ($fieldName == 'department')
					{
						$departmentId = 	$fieldValue->nsComplexObject_fields['internalId'];
					}
					$fieldValue = $fieldValue->nsComplexObject_fields['internalId'];
				}
				else if ($fieldValue->nsComplexObject_type == 'CustomFieldList')
				{
					$custFieldList = $fieldValue->nsComplexObject_fields;
					$customFields = $custFieldList[customField];
	
					foreach ($customFields as $customFieldsArrayItem)
					{
						if ($customFieldsArrayItem->nsComplexObject_type == 'StringCustomFieldRef')
						{
							$customFieldName = $customFieldsArrayItem->nsComplexObject_fields['internalId'];
							$customFieldValue = $customFieldsArrayItem->nsComplexObject_fields['value'];
							if ($customFieldName == 'custbody10')
							{
								$shipMemo = $customFieldValue;
							}
							else if ($customFieldName == 'custbody16')		//Customer Phone
							{
								$customerPhone = $customFieldValue;
							}
							else if ($customFieldName == 'custbody_alt_phone')		//Alternate Phone
							{
								$altPhone = $customFieldValue;
							}
						}
						else if ($customFieldsArrayItem->nsComplexObject_type == 'BooleanCustomFieldRef')
						{
							$customFieldName = $customFieldsArrayItem->nsComplexObject_fields['internalId'];
							$customFieldValue = $customFieldsArrayItem->nsComplexObject_fields['value'];
						}
						else if ($customFieldsArrayItem->nsComplexObject_type == 'SelectCustomFieldRef')
						{
							$customFieldName = $customFieldsArrayItem->nsComplexObject_fields['internalId'];
							$customFieldValue = $customFieldsArrayItem->nsComplexObject_fields['value'];
	
							if ($customFieldName == 'custbody5')		//Palletize (Yes or No) question
							{
								$palletized = isPalletized($customFieldValue);
							}
							else if ($customFieldName == 'custbody6')		//Premium Delivery Service
							{
								$premiumDelivery = getPremiumDelivery($customFieldValue);
							}
							else if ($customFieldName == 'custbody8')		//Signature Required (Yes or No) question
							{
								$signatureRequired = isSignatureRequired($customFieldValue);
							}
							else if ($customFieldName == 'custbody9')		//Liftgate (Yes or No) question
							{
								$liftGate = isLiftGate($customFieldValue);
							}
							else if ($customFieldName == 'custbody7')		//Level of Delivery Service
							{
								if ( $customFieldValue instanceof nsComplexObject )
								{
									if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
									{
										$customFieldListRecInternalId = $customFieldValue->nsComplexObject_fields['internalId'];
	
										$serviceType = getLookup("Level of Delivery Service", $customFieldListRecInternalId);
									}
								}
							}
							
						}
						else if ($customFieldsArrayItem->nsComplexObject_type == 'DoubleCustomFieldRef')
						{
							$customFieldName = $customFieldsArrayItem->nsComplexObject_fields['internalId'];
							$customFieldValue = $customFieldsArrayItem->nsComplexObject_fields['value'];
	
							if ($customFieldName == 'custbodyfreight_quote_1')
							{
								$freightQuote = $customFieldValue;
							}
						}
					}
				}
				else if ($fieldValue->nsComplexObject_type == 'ShipAddress')
				{
					$shipToName1 = $fieldValue->nsComplexObject_fields['shipAddressee'];
					$shipToAddress1 = $fieldValue->nsComplexObject_fields['shipAddr1'];
					$shipToAddress2 = $fieldValue->nsComplexObject_fields['shipAddr2'];
					$shipToCity = $fieldValue->nsComplexObject_fields['shipCity'];
					$shipToState = $fieldValue->nsComplexObject_fields['shipState'];
					$shipToZIP = $fieldValue->nsComplexObject_fields['shipZip'];
					$shipToCountry = $fieldValue->nsComplexObject_fields['shipCountry'];
					$shipToContact = $fieldValue->nsComplexObject_fields['shipAddressee'];
					$shipToPhone = $fieldValue->nsComplexObject_fields['shipPhone'];
					$shipAttention = $fieldValue->nsComplexObject_fields['shipAttention'];
	
					if ($shipAttention != null)
					{
						$shipToName2 = $shipToName1;	//Addressee will be printed in shipToName2
						$shipToName1 = $shipAttention;	//Attention will be printed in shipToName1
						$shipToContact = $shipAttention;
					}
	
					if ($shipToAddress1 != null && strlen($shipToAddress1) > 35)
					{
						$shipToAddress1 = substr($shipToAddress1,0, 35);
					}
					if ($shipToAddress2 != null && strlen($shipToAddress1) > 35)
					{
						$shipToAddress2 = substr($shipToAddress2,0, 35);
					}
					if ($shipToCity != null && strlen($shipToCity) > 35)
					{
						$shipToCity = substr($shipToCity,0, 35);
					}
					if ($shipToState != null && strlen($shipToState) > 35)
					{
						$shipToState = substr($shipToState,0, 35);
					}
					
					$shipToName1 = substr($shipToName1,0, 35);
					$shipToContact = substr($shipToContact,0, 35);
//					$shipToZIP = substr($shipToZIP, 0, 5);
					
					if ($shipToZIP == null)
					{
						$shipToZIP = '';
					}

				}
				else if ($fieldValue->nsComplexObject_type == 'SalesOrderItemList')
				{
					$salesOrderItemList = $fieldValue->nsComplexObject_fields;
				}
	
			}
			else
			{
	
				switch ($fieldName)
				{
				case 'tranId':
					$BLNumber  = $fieldValue;
					$soTranId = $fieldValue;
					break;
				}
			}
		}
	
		if ($shipMethodId != null)
		{
			$SCACcode = getLookup('SCAC Code', $shipMethodId);
		}
	
		if ($shipMethodId != null)
		{
			$departmentShortCode = getLookup('Department Short Code', $departmentId);
	
			if ($departmentShortCode != null)
			{
				$BLNumber = $BLNumber . '-' . $departmentShortCode;
				$shipperID = getLookup('EW Shipper', $departmentShortCode);
				$payerID = getLookup('EW Payer', $departmentShortCode);
			}
		}
	
		if ($shipToPhone == null)
		{
			$shipToPhone = $customerPhone;
		}
	
		$charsToReplace = array("(", ")","-"," ");
	
		$commentLinePhoneNo = $shipToPhone;
		$shipToPhone = str_replace($charsToReplace, '', $shipToPhone);
		$shipToPhone = substr($shipToPhone, 0, 14);
		$shipToCountry = getCountry($shipToCountry);
		$shipmentId++;
		
		$shipmentIdStr = str_pad($shipmentId,6,'0',STR_PAD_LEFT);
		
		//form the BOL Header string
		$BOLHeader = $recType;
		$BOLHeader = $BOLHeader . '|' . $BLNumber;
		$BOLHeader = $BOLHeader . '|' . $BLDate;
		$BOLHeader = $BOLHeader . '|' . $terms;
		$BOLHeader = $BOLHeader . '|' . $SCACcode;
		$BOLHeader = $BOLHeader . '|' . $proNumber;
		$BOLHeader = $BOLHeader . '|' . $shipmentIdStr;
		$BOLHeader = $BOLHeader . '|' . $shipToName1;
		$BOLHeader = $BOLHeader . '|' . $shipToName2;
		$BOLHeader = $BOLHeader . '|' . $shipToAddress1;
		$BOLHeader = $BOLHeader . '|' . $shipToAddress2;
		$BOLHeader = $BOLHeader . '|' . $shipToCity;
		$BOLHeader = $BOLHeader . '|' . $shipToState;
		$BOLHeader = $BOLHeader . '|' . $shipToZIP;
		$BOLHeader = $BOLHeader . '|' . $shipToCountry;
		$BOLHeader = $BOLHeader . '|' . $shipToPhone;
		$BOLHeader = $BOLHeader . '|' . $deptName;

		if ($salesOrderItemList != null)	//process the line items
		{
			$lineCount = 1;
			processSalesOrderLines($salesOrderItemList);
		}
	
		if ($commodityLinesArray == null)
		{
			Util::logMessage("Sales Order: " . $BLNumber . " No Line Items available to Process");
			return true;
		}
	
		writeToFile($BOLHeader, $datHandle);
	
		foreach ($commodityLinesArray as $commodityLine)
		{
			writeToFile($commodityLine, $datHandle);
		}
	
		createCommentLine($datHandle, $freightQuote, $serviceType, $shipMemo, $commentLinePhoneNo, $premiumDelivery, $signatureRequired, $liftGate, $altPhone);

		if ($shipToCountry == 'USA')
		{
			$shippingToUSA = true;
		}
		else
		{
			$shippingToUSA  = false;
		}

		return true;
	}		
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault Error while creating Expressworks file for the Sales Order with Internal ID: " + $soInternalId + ",SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError("Exception while creating Expressworks file for the Sales Order with Internal ID: " + $soInternalId + "," + $e->getMessage());
		return false;
	}
}


function processSalesOrderLines($salesOrderItemList)
{
	global $salesOrderItemCount;

	foreach ($salesOrderItemList as $listField => $listValue)
	{
		if ($listValue instanceof nsComplexObject )
		{
			$salesOrderItemCount = 1;					//single line item sales order
			processSalesOrderItemRecord($listValue);
		}
		else if (is_array($listValue))					//multi line item sales order
		{
			$salesOrderItemCount = count($listValue);
			foreach ($listValue as $salesOrderItemListElement)
			{
				$salesOrderItemRec = $salesOrderItemListElement;
				processSalesOrderItemRecord($salesOrderItemRec);
			}

		}
	}
}

function processSalesOrderItemRecord($salesOrderItemRec)
{
	global $quantity, $lineItemRate, $lineItemAmount, $integratedWith, $salesOrderItemType, $lineId;

	$custColFieldList = $salesOrderItemRec->nsComplexObject_fields['customFieldList'];
	$quantity = $salesOrderItemRec->nsComplexObject_fields['quantity'];
	$lineItemRate = $salesOrderItemRec->nsComplexObject_fields['rate'];
	$lineItemAmount = $salesOrderItemRec->nsComplexObject_fields['amount'];
	$createdPO = $salesOrderItemRec->nsComplexObject_fields['createdPo'];
	$lineId = $salesOrderItemRec->nsComplexObject_fields['line'];
	$itemMemberList = null;
	if ( $createdPO != null)
	{
		return;
	}
	foreach ($custColFieldList  as  $fieldName => $fieldValue)
	{
		if ($fieldValue == 'CustomFieldList')
		{
			$fieldsArray = $custColFieldList->nsComplexObject_fields['customField'];
			foreach ($fieldsArray as $fieldsArrayItem)
			{
				$fieldName = $fieldsArrayItem->nsComplexObject_fields['internalId'];
				if ($fieldName == 'custcol_item_type')
				{
					$salesOrderItemType = $fieldsArrayItem->nsComplexObject_fields['value'];
					if ( $salesOrderItemType == 'Assembly/Bill of Materials' || $salesOrderItemType == 'Inventory Item')
					{
						$itemRecRef = $salesOrderItemRec->nsComplexObject_fields['item'];
						$itemInternalId = $itemRecRef->nsComplexObject_fields['internalId'];
						$productRec = getProductRec($itemInternalId, $salesOrderItemType);
						$recFields = $productRec->nsComplexObject_fields;
						foreach ($recFields  as  $fieldName => $fieldValue)
						{
							if ( $fieldValue instanceof nsComplexObject )
							{
								if ($fieldValue->nsComplexObject_type == 'ItemMemberList')
								{
									$itemMemberList = $fieldValue->nsComplexObject_fields;
								}
							}
						}
						if ($itemMemberList != null)		//Assembly Item
						{
							processItemMembers($itemMemberList);
						}
						else if ($itemMemberList == null)		//Inventory Item
						{
							extractProductInfoForMalvern($productRec);
						}
						break;
					}
				}
			}
		}
	}

}


function processItemMembers($itemMemberList)
{
	global $quantity, $integratedWith;

	$itemMember = $itemMemberList['itemMember'];
	foreach ($itemMember as $itemMemberArrayItem)
	{
		$memberItemInternalId = $itemMemberArrayItem->nsComplexObject_fields['item']->nsComplexObject_fields['internalId'];
		$memberItemQuantity   = $itemMemberArrayItem->nsComplexObject_fields['quantity'];
		$quantity = $quantity * $memberItemQuantity;
		$memberProductRec = getProductRec($memberItemInternalId, 'Inventory Item');
		extractProductInfoForMalvern($memberProductRec);
	}
}

function extractProductInfoForMalvern($productRec)
{
	//Commidity variables

	global $lineCount, $palletized, $quantity, $commodityLinesArray, $lineItemRate, $lineItemAmount, $lineId, $shipMethodId, $vendorPurchasePrice;

	$commodityLine = '';

	$recType = '2 ';
	$commodity = '';
	$numberOfHandlingUnits = '1';
	$handlingUnitsType = '';
	$numberOfPieces = '';
	$piecesType = '';
	$description = '';
	$NMFCNumber = '';
	$NMFCClassification;
	$weight = '';
	$weightUnitofMeasure = 'EA';
	$rate = '0';
	$charge = '0';
	$foodItemFlag = '0';
	$chemicalItemFlag = '0';
	$lowerTempLimit = '';
	$upperTempLimit = '';
	$tempUnitOfMeasure = '';
	$usedEquipmentIndicator = '0';
	$declaredValue = '';
	$valuePerUnit = '';
	$harmonizeCodeForIntlShipment = '';
	$countryOfManufacture = '';
	$billWeight = '';
	$cubicFeet = '';
	$hazardousMaterialCode = '';
	$$hazardousMaterialProperShippingName = '';
	$hazardousMaterialClassCode = '';
	$hazardousMaterialPackagingGroupCode = '';
	$additionalHazardousMaterialDescription = '';
	$hazardousMaterialTechnicalName = '';
	$specialHandlingCode1 = '';
	$specialHandlingCode2 = '';
	$specialHandlingCode3 = '';
	$length  = '';
	$width  = '';
	$height  = '';
	$averageCost = 0;
	$lastPurchasePrice = 0;
	
	$recFields = $productRec->nsComplexObject_fields;
	$countryOfManufacture = $recFields['countryOfManufacture'];
	if  ($countryOfManufacture != null)
	{
		$countryOfManufacture = getCountry($countryOfManufacture);
	}
	else
	{
		$countryOfManufacture = '';
	}
	foreach ($recFields  as  $fieldName => $fieldValue)
	{
		if ( $fieldValue instanceof nsComplexObject )
		{
			if ($fieldValue->nsComplexObject_type == 'RecordRef')
			{
				$fieldValue = $fieldValue->nsComplexObject_fields['internalId'];
			}
			else if ($fieldValue->nsComplexObject_type == 'PricingMatrix')
			{
				$pricingArray = $fieldValue->nsComplexObject_fields['pricing'];
				getbasePrice($pricingArray);
			}
			else if ($fieldValue->nsComplexObject_type == 'ItemVendorList')
			{
				$itemVendorArray = $fieldValue->nsComplexObject_fields;
				getVendorPurchaseCost($itemVendorArray);
			}
			else if ($fieldValue->nsComplexObject_type == 'CustomFieldList')
			{
				$custFieldList = $fieldValue->nsComplexObject_fields;
				$customFields = $custFieldList[customField];

				foreach ($customFields as $customFieldsArrayItem)
				{
					if ($customFieldsArrayItem->nsComplexObject_type == 'StringCustomFieldRef')
					{
						$customFieldName = $customFieldsArrayItem->nsComplexObject_fields['internalId'];
						$customFieldValue = $customFieldsArrayItem->nsComplexObject_fields['value'];

						switch ($customFieldName)
						{
						case 'custitemnmrc':
							$NMFCNumber = $customFieldValue;
							$NMFCNumber = str_replace('-', '',$NMFCNumber);
							break;
						case 'custitemfrtclass':
//							$NMFCClassification = str_replace('.', '', $customFieldValue);	//removed as per email from Renee on 5/17/11
							$NMFCClassification = $customFieldValue;
							break;
						case 'custitemharmonized_code':
							$harmonizeCodeForIntlShipment = $customFieldValue;
							break;
						}
					}
					else if ($customFieldsArrayItem->nsComplexObject_type == 'DoubleCustomFieldRef')
					{
						$customFieldName = $customFieldsArrayItem->nsComplexObject_fields['internalId'];
						$customFieldValue = $customFieldsArrayItem->nsComplexObject_fields['value'];

						if ($customFieldName == 'custitemcarton_length')
						{
							$length = $customFieldValue;
						}
						else if ($customFieldName == 'custitemcarton_width')
						{
							$width = $customFieldValue;
						}
						else if ($customFieldName == 'custitemcarton_height')
						{
							$height = $customFieldValue;
						}
					}					
					else if ($customFieldsArrayItem->nsComplexObject_type == 'SelectCustomFieldRef')
					{

					}
				}
			}
		}
		else
		{

			switch ($fieldName)
			{
			case 'mpn':
				$commodity  = $fieldValue;
				break;
			case 'salesDescription':
				$description  = $fieldValue;
				break;
			case 'weight':
				$weight  = $fieldValue;
				break;
			case 'weightUnit':
				$weightUnitofMeasure  = $fieldValue;
				break;
			case 'averageCost':
				$averageCost  = $fieldValue;
				break;
			case 'lastPurchasePrice':
				$lastPurchasePrice = $fieldValue;
				break;
			}
		}
	}

//	$weightUnitofMeasure = getWeightUnits($weightUnitofMeasure);

	if ($palletized == 'No')
	{
		$handlingUnitsType = 'CTN';
		$numberOfHandlingUnits = $quantity;
//		$piecesType = '';
//		$numberOfPieces = '';
		$piecesType = 'CTN';
		$numberOfPieces = $numberOfHandlingUnits;
		$weight = $weight * $quantity;
	}
	else if ($palletized == 'Yes')
	{
		if ($lineCount == 1)				//for Assembly Items - if this is the 1st line
		{
			$handlingUnitsType = 'PLT';
			$numberOfHandlingUnits = 1;
			$piecesType = 'CTN';
			$numberOfPieces = $quantity;
			$weight = $weight * $quantity;
//			$weight = $weight + 30;
			$weight = $weight + 40;	//modified per case 25882
		}
		else
		{
			$handlingUnitsType = '';
			$numberOfHandlingUnits = '';
			$piecesType = 'CTN';
			$numberOfPieces = $quantity;
			$weight = $weight * $quantity;
		}
	}

	if ($commodity != null)
	{
		$description = $commodity . '-' . $description ;
	}

	if ($averageCost == null)
	{
		$averageCost = 0;
	}

	if ($lastPurchasePrice == null)
	{
		$lastPurchasePrice = 0;
	}

	if ($vendorPurchasePrice == null)
	{
		$vendorPurchasePrice = 0;
	}
	
	$weightUnitofMeasure = 'EA';

	if ($averageCost != 0)
	{
		$declaredValue = $averageCost * $quantity;
	}
	else if ($lastPurchasePrice != 0)
	{
		$declaredValue = $lastPurchasePrice * $quantity;
	}
	else if ($vendorPurchasePrice != 0)
	{
		$declaredValue = $vendorPurchasePrice * $quantity;
	}
	
	$pos = strpos($declaredValue ,".");
	if ($pos === false)
	{
		$declaredValue = $declaredValue . '.00'; 
	}

	$valuePerUnit = 'FR';

	$commodityLine = $recType;
	$commodityLine = $commodityLine . '|' . $commodity;
	$commodityLine = $commodityLine . '|' . $numberOfHandlingUnits;
	$commodityLine = $commodityLine . '|' . $handlingUnitsType;
	$commodityLine = $commodityLine . '|' . $numberOfPieces;
	$commodityLine = $commodityLine . '|' . $piecesType;
	$commodityLine = $commodityLine . '|' . $description;
	$commodityLine = $commodityLine . '|' . $NMFCNumber;
	$commodityLine = $commodityLine . '|' . $NMFCClassification;
	$commodityLine = $commodityLine . '|' . $weight;
	$commodityLine = $commodityLine . '|' . $weightUnitofMeasure;
	/*
	if ($shipMethodId != '2')	//Not Equal to FedEx Ground
	{
		$commodityLine = $commodityLine . '|' . $declaredValue;	
	}
	else
	{
		$commodityLine = $commodityLine . '|' . '';	
	}
	*/
	$commodityLine = $commodityLine . '|$' . $declaredValue;	
	
	$commodityLine = $commodityLine . '|' . $countryOfManufacture;
	$commodityLine = $commodityLine . '|' . $lineId;
	$commodityLine = $commodityLine . '|' . $harmonizeCodeForIntlShipment;
	$commodityLine = $commodityLine . '|' . $length;
	$commodityLine = $commodityLine . '|' . $width;
	$commodityLine = $commodityLine . '|' . $height;
	
	$commodityLinesArray[] = $commodityLine;

	$lineCount = $lineCount + 1;

}

function getProductRec($intId, $itemType)
{
	try
	{
		global $myNSclient;
		if ($itemType == 'Inventory Item')
		{
			$productRef = new nsRecordRef(array( "internalId" => $intId, "type" => "inventoryItem"));
		}
		else if ($itemType == 'Assembly/Bill of Materials')
		{
			$productRef = new nsRecordRef(array( "internalId" => $intId, "type" => "assemblyItem"));
		}

		$getResp = $myNSclient->get($productRef);
		return $getResp->record;
	}
	catch (SoapFault $soapFault)
	{
		//Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		//Util::logError($e->getMessage());
		return false;
	}

}

function createCommentLine($datHandle, $freightQuote, $serviceType, $shipMemo, $commentLinePhoneNo, $premiumDelivery, $signatureRequired, $liftGate, $altPhone)
{
	$commentLine = '';
	$recType = '3 ';
	$comments = '';
	
	$commentLine = $recType;
	$commentLine = $commentLine . '|' . $freightQuote;
	$commentLine = $commentLine . '|' . $serviceType;
	if ($commentLinePhoneNo == null)
	{
		$commentLine = $commentLine . '|' . $shipMemo;
	}
	else
	{
		$commentLine = $commentLine . '|' . $shipMemo . '-' . $commentLinePhoneNo;
	}
	$commentLine = $commentLine . '|' . $premiumDelivery;
	$commentLine = $commentLine . '|' . 'SIGNATURE=' . $signatureRequired;
	$commentLine = $commentLine . '|' . 'LIFTGATE=' . $liftGate;
	$commentLine = $commentLine . '|' . 'Alternate Phone Number:' . $altPhone;
	
	writeToFile($commentLine, $datHandle);
}

function getLookup($lookupName, $lookupField)
{
	global $myNSclient;

//	$custFldSrch - new nsComplexObject("CustomFieldList");

	$recTypeSrch = new nsComplexObject("RecordRef");
	$recTypeSrch->setFields(array("internalId" => "143"));

	$nameSrch = new nsComplexObject("SearchStringField");
	$nameSrch->setFields( array("searchValue" => $lookupName , "operator" => "is") );
	$lookupSearch = new nsComplexObject("CustomRecordSearchBasic");
	$lookupSearch->setFields(array("name" => $nameSrch,  "recType" => $recTypeSrch));

	// Invoke search operation
	$searchResponse = $myNSclient->search($lookupSearch);
	if (!$searchResponse->isSuccess)
	{
		return;
	}
	$recPos = 0;
	//while ($searchResponse->pageIndex <= $searchResponse->totalPages)
	//{
		foreach ($searchResponse->recordList as $rec)
		{
			$recFields = $rec->nsComplexObject_fields;
			$custFields = $recFields["customFieldList"];
			$custFieldList = $custFields->nsComplexObject_fields["customField"];
			$value = "";
			$field = "";
			foreach ($custFieldList as $custFieldRec)
			{
				$fieldName = $custFieldRec->nsComplexObject_fields["internalId"];
				if ($custFieldRec->nsComplexObject_type == 'StringCustomFieldRef' && $fieldName == "custrecord_ship_int_lookup_value")
				{
					$value = $custFieldRec->nsComplexObject_fields["value"];
				}
				else if ($custFieldRec->nsComplexObject_type == 'StringCustomFieldRef' && $fieldName == "custrecord_ship_int_lookup_field")
				{
					$field = $custFieldRec->nsComplexObject_fields["value"];
				}
			}
			if ($field == $lookupField )
			{
				return $value;
				break;
			}
		}
	//}
	return '';
}

function writeToFile($lineStr,$datHandle)
{
	$lineStr = $lineStr . "\r\n";
	fwrite($datHandle, $lineStr);
}

function getCountry($enumeratedCountry)
{
	global $integratedWith;
	$country = "";
	switch ($enumeratedCountry)
	{
	case '_unitedStates':
		$country  = "USA";
		return $country;
		break;
	case '_canada':
		$country  = "CA";
		return $country;
		break;
	case '_china':
		$country  = "CN";
		return $country;
		break;
	default:
		return $country;
	}
}

function getWeightUnits($enumeratedUnits)
{
	$weightUnit = "";
	switch ($enumeratedUnits)
	{
	case '_lb':
		$weightUnit  = "LB";
		return $weightUnit;
		break;
	case '_kg':
		$weightUnit  = "KG";
		return $weightUnit;
		break;
	default:
		return $weightUnit;
	}
}

function isPalletized($customFieldValue)
{
	$palletize = "";
	if ( $customFieldValue instanceof nsComplexObject )
	{
		if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
		{
			$customFieldListRecInternalId = $customFieldValue->nsComplexObject_fields['internalId'];
			if ($customFieldListRecInternalId == '1')
			{
				$palletize = 'Yes';
			}
			else if ($customFieldListRecInternalId == '2')
			{
				$palletize = 'No';
			}
			return $palletize;
		}
	}
}

function getPremiumDelivery($customFieldValue)
{

//	  return "";
//	  global $myNSclient
	  global $myNSclient, $customListResp;
	  $typeId = $customFieldValue->nsComplexObject_fields['typeId'];
      $intId = $customFieldValue->nsComplexObject_fields['internalId'];

      try
      {
//            $customListResp = $myNSclient->getCustomization('customList');
            foreach ($customListResp->recordList as $rec)
            {
                  $recFields = $rec->nsComplexObject_fields;
                  if ($recFields['internalId'] == $typeId)
                  {
                        $custList = $recFields['customValueList']->nsComplexObject_fields['customValue'];
                        foreach ($custList as $values)
                        {
                              $custVal = $values->nsComplexObject_fields;
                              if ($custVal['valueId'] == $intId)
                              {
                                    return $custVal['value'];
                              }
                        }
                  }
            }
      }
      catch (SoapFault $soapFault)
      {
            Util::logError("SOAP Fault :" . $soapFault->faultstring);
            return "";
      }
      catch (Exception $e) {
            Util::logError($e->getMessage());
            return "";
      }
      return "";
}

function isSignatureRequired($customFieldValue)
{
	$signatureRequired = "";
	if ( $customFieldValue instanceof nsComplexObject )
	{
		if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
		{
			$customFieldListRecInternalId = $customFieldValue->nsComplexObject_fields['internalId'];
			if ($customFieldListRecInternalId == '1')
			{
				$signatureRequired = 'Y';
			}
			else if ($customFieldListRecInternalId == '2')
			{
				$signatureRequired = 'N';
			}
			return $signatureRequired;
		}
	}
}

function isLiftGate($customFieldValue)
{
	$liftGate = "";
	if ( $customFieldValue instanceof nsComplexObject )
	{
		if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
		{
			$customFieldListRecInternalId = $customFieldValue->nsComplexObject_fields['internalId'];
			if ($customFieldListRecInternalId == '1')
			{
				$liftGate = 'Y';
			}
			else if ($customFieldListRecInternalId == '2')
			{
				$liftGate = 'N';
			}
			return $liftGate;
		}
	}
}

function updateProcessed($intId)
{
	try
	{
		global $myNSclient, $shippingToUSA;

		$flagFieldRef = new nsComplexObject("BooleanCustomFieldRef");
		$flagFieldRef->setFields(array("internalId" => "custbody_ship_int_flag", "value" => true));
		$custFldList = new nsComplexObject("CustomFieldList");
		$custFldList->setFields(array("customField" => $flagFieldRef));
		$updRec = new nsComplexObject("SalesOrder");
		$updRec->setFields(array("internalId" => $intId, "customFieldList" => $custFldList ));

		$updResp = $myNSclient->update($updRec);
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return false;
	}

}

function updateProcessedMalvern($intId, $soTranId, $datHandle2)
{
	try
	{
		global $myNSclient;

		$flagFieldRef = new nsComplexObject("BooleanCustomFieldRef");
		$flagFieldRef->setFields(array("internalId" => "custbody_malvern_extract_processed", "value" => true));
		$custFldList = new nsComplexObject("CustomFieldList");
		$custFldList->setFields(array("customField" => $flagFieldRef));
		$updRec = new nsComplexObject("SalesOrder");
		$updRec->setFields(array("internalId" => $intId, "customFieldList" => $custFldList ));

		$updResp = $myNSclient->update($updRec);
		
		if (!$updSOResp->isSuccess)
		{
			if ($updResp->statusDetail[0]->type == 'ERROR')
			{
				$errorStr = "Error when updating Sales Order " . $soTranId . " : " . $updResp->statusDetail[0]->message . "\r\n";
				fwrite($datHandle2, $errorStr);
			}
			return false;
		}
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return false;
	}

} 
function checkBackOrder($itemList)
{

	foreach ($itemList as $listField => $listValue)
	{
		if ($listValue instanceof nsComplexObject )
		{
			if ($listValue->nsComplexObject_fields["quantityBackOrdered"] > 0)
			{
				return true;
			}
		}
		else if (is_array($listValue))					//multi line item sales order
		{
			foreach ($listValue as $item)
			{
				if ($item->nsComplexObject_fields["quantityBackOrdered"] > 0)
				{
					return true;
				}

			}
		}
	}

	return false;
}

function getbasePrice($pricingArray)
{
	global $lineItemRate;

	foreach ($pricingArray as $pricingArrayItem)
	{
		if ($pricingArrayItem->nsComplexObject_fields['priceLevel']->nsComplexObject_fields['name'] == 'Base Price')
		{
			$priceArray = $pricingArrayItem->nsComplexObject_fields['priceList']->nsComplexObject_fields['price'];
			$lineItemRate = $priceArray[0]->nsComplexObject_fields['value'];
		}
	}
}

function getVendorPurchaseCost($itemVendorArray)
{
	global $vendorPurchasePrice;

	foreach ($itemVendorArray as $itemVendorArrayItems)
	{
		if ($itemVendorArrayItems instanceof nsComplexObject )
		{
			$vendorPurchasePrice = $itemVendorArrayItems->nsComplexObject_fields['purchasePrice'];
			if ($vendorPurchasePrice != null)
			{
				break;
			}
		}
		else if (is_array($itemVendorArrayItems))
		{
			foreach ($itemVendorArrayItems as $itemVendorArrayItem)
			{
				$vendorPurchasePrice = $itemVendorArrayItem->nsComplexObject_fields['purchasePrice'];
				if ($vendorPurchasePrice != null)
				{
					break;
				}
			}

		}
	}
}

function getCompanyName($intId)
{
	try
    {
    	global $myNSclient;
        $recRef = new nsRecordRef(array( "internalId" => $intId, "type" => "customer"));
        $getResp = $myNSclient->get($recRef);
        $custRec = $getResp->record;
        if ($custRec->nsComplexObject_fields['companyName'] == null)
        {
        	return "";
        }
        else
        {
        	return $custRec->nsComplexObject_fields['companyName'];
        }
	}
    catch (SoapFault $soapFault)
    {
    	Util::logError("SOAP Fault :" . $soapFault->faultstring);
        return "";
    }
    catch (Exception $e) {
    	Util::logError($e->getMessage());
        return "";
	}
}

function checkMultiLineProcessed($custFieldList)
{
	foreach ($custFieldList as $customFieldsArrayItem)
	{
		if ($customFieldsArrayItem->nsComplexObject_type == 'BooleanCustomFieldRef')
		{
			$customFieldName = $customFieldsArrayItem->nsComplexObject_fields['internalId'];
			$customFieldValue = $customFieldsArrayItem->nsComplexObject_fields['value'];
//			if ($customFieldName == 'custbody_ship_int_flag')		//integration flag
			if ($customFieldName == 'custbody_malvern_extract_processed')		//integration flag
			{
				if ($customFieldValue == true)
				{
					return true;
				}
			}

		}
	}
}

function checkMultiLine($itemList)
{
	$returnFlag = false;
	$inventoryItemQty = 0;
    foreach ($itemList as $listField => $listValue)
    {
    	if ($listValue instanceof nsComplexObject )
		{
			if ($listValue->nsComplexObject_fields["quantity"] > 1  || isAssemblyItem($listValue) == 'A' )
			{
            	$returnFlag = true;
			}
		}
		else if (is_array($listValue))                                                                         //multi line item sales order
		{
			foreach ($listValue as $item)
			{
				if ((isAssemblyItem($item) == 'I' && $item->nsComplexObject_fields["quantity"] > 1)   || isAssemblyItem($item) == 'A')
				{
					$returnFlag = true;
				}
				else if (isAssemblyItem($item) == 'I' && $item->nsComplexObject_fields["quantity"] == 1)
				{
					$inventoryItemQty = $inventoryItemQty + 1;
				}
			}
			if ($inventoryItemQty > 1)
			{
				$returnFlag = true;
			}
		}
	}
	return $returnFlag;
}

function isAssemblyItem($itemRec)
{
	$returnFlag = '';
	$custColFieldList = $itemRec->nsComplexObject_fields['customFieldList'];
	foreach ($custColFieldList  as  $fieldName => $fieldValue)
	{
		if ($fieldValue == 'CustomFieldList')
		{
			$fieldsArray = $custColFieldList->nsComplexObject_fields['customField'];
			foreach ($fieldsArray as $fieldsArrayItem)
			{
				$fieldName = $fieldsArrayItem->nsComplexObject_fields['internalId'];
				if ($fieldName == 'custcol_item_type')
				{
					$salesOrderItemType = $fieldsArrayItem->nsComplexObject_fields['value'];
					if ($salesOrderItemType == 'Assembly/Bill of Materials')
					{
						$returnFlag = 'A';
					}
					else if ($salesOrderItemType == 'Inventory Item')
					{
						$returnFlag = 'I';
					}
				}
			}
		}
	}
	return $returnFlag;
}

function endProcess() {
	//Util::logMessage("Process Ended successfully");
	//Util::close();
}
$totRecs = 0;
$errRecs = 0;
$succRecs = 0;
$configFile = $argv[1];
$logFileName = $argv[2];
$dataDir = $argv[3];
/*
$configFile = '';//$argv[1];
$logFileName = '';//$argv[2];
$dataDir = '.';//$argv[3];
*/
$datHandle = "";

startProcess($configFile,$logFileName,$dataDir );
endProcess();


?>
