<?php
require_once('common/NSPHPtoolkit.php');
require_once('common/class_Util.php');

function startProcess($configFile, $logFileName)
{
	global $myNSclient,$totRecs,$errRecs,$succRecs,$inputLogFile,$datFile1;

	try
	{
		if ($configFile == '')
			$configFile = "login_params.xml";
			
		Util::init($configFile,$logFileName);
		Util::logMessage("Malvern to Netsuite Shipment integration started ....");
		$myNSclient = new nsClient( nsHost::live );

		$loginResp = $myNSclient->login(Util::$loginSXML->NSuserName, Util::$loginSXML->NSpassword, Util::$loginSXML->NSaccount, Util::$loginSXML->NSroleKey);
		if (!$loginResp->isSuccess)
		{
			Util::logError("Error logging into Netsuite...");
			return false;
		}
		$myNSclient->setSearchPreferences(false,500,true);
		$myNSclient->setPreferences(true);
		defineFieldsListArray();
		processTransaction();
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return false;
	}
}

function processTransaction()
{
	try
	{
		global $inboundFile, $myNSclient,$totRecs,$errRecs,$succRecs, $transactionFieldValuesArray, $transactionLineFieldValuesArray, $dataDir, $datFile1, $datFile2, $datHandle1, $datHandle2, $transactionFieldsListArray, $transactionLineFieldsListArray;
		
		$trackingNumberArray = array();
		$mpnArray = array();
		$lineItemCodeArray  = array();
		$trackingNumberCount = 0;
		$mpnCount = 0;
		$lineItemCodeCount = 0;
		
		//$inboundFile = Util::$appDataSXML->malvernFulfilFilePath . '/' .Util::$appDataSXML->malvernFulfilFile;

		if (!file_exists($inboundFile))
		{
			Util::logError("Malvern Fulfillment file " . $inboundFile . "does not exist.");
		    return;
		}		
		
		$contents = file($inboundFile);
		
		if ($contents == null)
		{
			Util::logError("Malvern Fulfillment file " . $inboundFile . "does not contain any data.");
		    return;
		}
		$charsToReplace = '\r\n';
		$orderNumber = '';
		$shipDate = '';
		$scacCode = '';
		$oldOrderNumber = '';
		$oldTrackNo = '';
		$quoteAmt = 0;
		$trackingNumberArray = array();
		$mpnArray = array();
		$lineItemCodeArray  = array();		
		for ($i=0; $i < sizeof($contents); $i++) 
		{
			$line = trim($contents[$i]);
			$line = str_replace($charsToReplace, '', $line);
			$lineValues = explode("|", $line);
			$orderNumber = $lineValues[0];
			if ($oldOrderNumber != '' && $oldOrderNumber != $orderNumber)
			{
				$retVal = processOrder($oldOrderNumber, $shipDate, $scacCode,$trackingNumberArray, $lineItemCodeArray,$mpnArray, $quoteAmt);
				if ($retVal)
				{
					$succRecs++;
				}
				else
				{ 
					$errRecs++;
					writeOrderDetails($orderNumber, $shipDate, $scacCode, $trackingNumberArray, $lineItemCodeArray, $mpnArray, $quoteAmt);
				}			
				
				$trackingNumberArray = array();
				$mpnArray = array();
				$lineItemCodeArray  = array();
				$quoteAmt = 0;
			}
			$oldOrderNumber = $lineValues[0];
			$shipDate  = $lineValues[1];
			$scacCode  = $lineValues[2];
			$trackingNo = $lineValues[3];
			$mpn = $lineValues[4];
			$lineItemCode = $lineValues[5];
			if ($trackingNo != $oldTrackNo)
				$quoteAmt += floatval($lineValues[6]);
			$oldTrackNo = $trackingNo;
			
			array_push($lineItemCodeArray,$lineItemCode);
			array_push($mpnArray,$mpn);
			array_push($trackingNumberArray,$trackingNo);

		}
		if ($orderNumber != "")
		{
			$retVal = processOrder($oldOrderNumber, $shipDate, $scacCode,$trackingNumberArray, $lineItemCodeArray,$mpnArray, $quoteAmt);
			if ($retVal)
			{
				$succRecs++;
			}
			else
			{ 
				$errRecs++;
				writeOrderDetails($orderNumber, $shipDate, $scacCode, $trackingNumberArray, $lineItemCodeArray, $mpnArray, $quoteAmt);
			}			
			
		}
		return true;
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return false;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return false;
	}
}
function writeOrderDetails($orderNumber, $shipDate, $scacCode,$trackingNumberArray, $lineItemCodeArray,$mpnArray,$quoteAmt)
{
	Util::logMessage("Ship Date : " . $shipDate . ", SCAC Code : " . $scacCode . ", Quote amount : " . $quoteAmt );
	for ($intPos = 0; $intPos < count($lineItemCodeArray); $intPos++)
	{
		Util::logMessage("Line # : " . $lineItemCodeArray[$intPos] . ",MPN : " . $mpnArray[$intPos] . ",Tracking Number : " . $trackingNumberArray[$intPos] );
	}
	Util::logMessage("");
}
function getOrderId($orderNumber)
{
	global $myNSclient;
	$transactionSearch = new nsComplexObject("TransactionSearchBasic");
	
	$tranSrchString = new nsComplexObject("SearchStringField");
	$tranSrchString->setFields(array("searchValue" => $orderNumber, "operator" => "is"));
	
	$tranTypeSrch = new nsComplexObject("SearchEnumMultiSelectField");
	$tranTypeSrch->setFields(array("searchValue" => array("_salesOrder"), "operator" => "anyOf"));
	
	$transactionSearch->setFields(array("tranId" => $tranSrchString, "type" => $tranTypeSrch));

	$searchResponse = $myNSclient->search($transactionSearch);

	if (!$searchResponse->isSuccess)
	{
		Util::logError(" Error searching of Order in Netsuite :  Error : " . $searchResponse->statusDetail[0]->message);
		return null;
	}
	if ($searchResponse->totalRecords <= 0)
	{
		Util::logMessage("No orders found to process.");
		return null;
	}
	
	if ($searchResponse->totalRecords > 1)
	{
		Util::logError("More than one sales order found in Netsuite for Order Number: " . $orderNumber); 
		return null;
	}

	return $searchResponse->recordList[0]->getField('internalId');
}
function processOrder($orderNumber, $shipDate, $scacCode,$trackingNumberArray, $lineItemCodeArray,$mpnArray,$quoteAmt)
{
	global $myNSclient,$lineValuesList;
	global $totRecs, $errRecs, $succRecs;
	
	$totRecs++;
	try
	{
		$lineItemCodeArray = array_unique($lineItemCodeArray);
		//if (count($lineItemCodeArray) > 1)
		//{
		//	Util::logError("More than one line in the order, could not process order : ". $orderNumber ); 
		//	return false;
		//}
		$trackingNumberArray = array_unique($trackingNumberArray);
		
		$splitOrderNumber = explode("-",$orderNumber) ;
		$orderNumber = $splitOrderNumber[0];
		$orderId = getOrderId($orderNumber);
		if ($orderId == null)
		{
			Util::logError(" Could not find the order in Netsuite for : " . $orderNumber  );
			return false;
		}
		$shipMethodId = getShipMethod($scacCode);
		if ($shipMethodId == null || $shipMethodId == '')
		{
			Util::logError(" Could not find the ship method id for SCAC Code : " . $scacCode + ", order : " . $orderNumber  );
			return false;
		}
		$itemFulFillRec = getItemFulfillment($orderId);
		if ($itemFulFillRec == null)
		{
			Util::logError(" Error creating fulfillmet in Netsuite for Order : " . $orderNumber  );
			return false;
		}
		$headerValues = getFieldValues($itemFulFillRec);
		$itemFulFillLineList = array();
		foreach ($lineItemCodeArray as $prodRec)
		{
			if ($prodRec == '')
			{
				Util::logError(" Order Line is blank for fulfillment in Order : " . $orderNumber );
				return false;				
			}
			$lineRec = checkForValidLine($lineValuesList, $prodRec) ;
			if ($lineRec == null)
			{
				Util::logError(" Order Line : " . $prodRec . ", not available for fulfillment in Order : " . $orderNumber );
				return false;
			}
			//unset($lineRec->nsComplexObject_fields['location'] );
			array_push($itemFulFillLineList, $lineRec);
		}
		
		// Update sales order before processing creating fulfillment
		
		$customFields = array();	
		$soCustomFieldList = new nsComplexObject("CustomFieldList");
		
		$customFieldRec = new nsComplexObject("DoubleCustomFieldRef");
		$customFieldRec->setFields(array("internalId" => 'custbodyfreight_quote_1', "value" => $quoteAmt));
		array_push($customFields,$customFieldRec);
				
		$soCustomFieldList->setFields(array("customField" => $customFields));	
		
		$soRec = new nsComplexObject("SalesOrder");
		$soRec->setFields(array('internalId' => $orderId, 'customFieldList' => $soCustomFieldList));
		if ($shipMethodId != null && $shipMethodId != '')
		{
			$soRec->setFields(array('shipMethod' => new nsRecordRef(array('internalId' => $shipMethodId))));
		}		
		$updSoResp = $myNSclient->update($soRec);
		if (!$updSoResp->isSuccess)
		{
			Util::logError("Error updating sales order , " . $updSoResp->statusDetail[0]->message . ", for order # : " . $orderNumber);
			return false;
		}		
		// Create fulfillment
			
		$itemFulfillRecList = new nsComplexObject("ItemFulfillmentItemList");
		$itemFulfillRecList->setFields(array("item" => $itemFulFillLineList));
		
		$itemFulFillPkgRecs = array();
		$pkgRecType = "";
		foreach ($trackingNumberArray as $trackNoRec)
		{
			//if ($itemFulFillRec->nsComplexObject_fields['shipmentWeightUps'] != null)
			//{
			//	$pkgRecType = "Ups";
			//}
			$itemFulFillPkgRec = new nsComplexObject("ItemFulfillmentPackage" . $pkgRecType);
			$itemFulFillPkgRec->setFields(array('packageTrackingNumber'  . $pkgRecType => $trackNoRec, 'packageWeight'  . $pkgRecType => 1 ));
			array_push($itemFulFillPkgRecs, $itemFulFillPkgRec);			
		}
		$itemFulfillPkgList = new nsComplexObject("ItemFulfillmentPackage"  . $pkgRecType . "List");
		$itemFulfillPkgList->setFields(array("package"  . $pkgRecType=> $itemFulFillPkgRecs));	
		$itemFulFillRec->setFields(array("package"  . $pkgRecType . "List" => $itemFulfillPkgList));
	
		$customFields = array();
		$itemFulFillCustomFieldList = new nsComplexObject("CustomFieldList");	
		/*
		$itemFulFillCustomFieldList = $itemFulFillRec->nsComplexObject_fields['customFieldList'];
		if ($itemFulFillCustomFieldList == null)
		{
			$itemFulFillCustomFieldList = new nsComplexObject("CustomFieldList");
		}
		else
		{
			$customFields = $itemFulFillCustomFieldList->nsComplexObject_fields['customField'];
			if (!is_array($customFields))
			{
				$customFieldRec = $customFields;
				$customFields = array();
				array_push($customFields,$customFieldRec);			
			}
		}
				
		$itemFulFillCustomFieldList->setFields(array("customField" => $customFields));
		*/
			
		$customFieldRec = new nsComplexObject("DoubleCustomFieldRef");
		$customFieldRec->setFields(array("internalId" => 'custbodyfrt_exp', "value" => $quoteAmt));
		array_push($customFields,$customFieldRec);
				
		$customFieldRec = new nsComplexObject("StringCustomFieldRef");
		$customFieldRec->setFields(array("internalId" => 'custbody_fulfill_lines', "value" => implode(",", $lineItemCodeArray)));
		array_push($customFields,$customFieldRec);
				
		$itemFulFillCustomFieldList->setFields(array("customField" => $customFields));	
		
		$itemFulFillRec->setFields(array("tranDate" => convert2NSDate($shipDate), "itemList" => $itemFulfillRecList, "customFieldList" => $itemFulFillCustomFieldList));

		/*
		if ($shipMethodId != null && $shipMethodId != '')
		{
			$itemFulFillRec->setFields(array('shipMethod' => new nsRecordRef(array('internalId' => $shipMethodId))));
		}
		$shipAddr = $itemFulFillRec->nsComplexObject_fields['transactionShipAddress'];
		$addr2 = $shipAddr->nsComplexObject_fields['shipAddr2'];
		$shipCompany = $shipAddr->nsComplexObject_fields['shipAddressee'];
		if ($addr2 != null && $addr2 != '' && strlen($addr2) > 35)
		{
			$addr2 = substr($addr2,0,34);
		}
		if ($shipCompany != null && $shipCompany != '' && strlen($shipCompany) > 35)
		{
			$shipCompany = substr($shipCompany,0,34);
		}
		$shipAddr->setFields(array("shipAddr2" => $addr2, "shipAddressee" => $shipCompany) );
		$itemFulFillRec->setFields(array("transactionShipAddress" => $shipAddr) );
		unset($itemFulFillRec->nsComplexObject_fields['entity'] );
		unset($itemFulFillRec->nsComplexObject_fields['createdDate'] );
		unset($itemFulFillRec->nsComplexObject_fields['lastModifiedDate'] );
		unset($itemFulFillRec->nsComplexObject_fields['shipmentWeightUps'] );
		unset($itemFulFillRec->nsComplexObject_fields['shipAddress'] );
		*/		
		
		$addResp = $myNSclient->add($itemFulFillRec);
		if (!$addResp->isSuccess)
		{
			Util::logError("Error creating item fulfillment , " . $addResp->statusDetail[0]->message . ", for order # : " . $orderNumber);
			return false;
		}

		return true;
	}
	catch (SoapFault $soapFault)
	{
		$errRecs++;
		Util::logError("SOAP Fault in processOrder :" . $soapFault->faultstring . ", for order # : " . $orderNumber );
		return false;
	}
	catch (Exception $e) {
		$errRecs++;
		Util::logError($e->getMessage());
		return false;
	}
}

function checkForValidLine($lineList, $prodLine)
{
	global $itemList;
	$itemRecsList = $itemList['item'];
	if (array_key_exists(0,$itemRecsList))
	{					
		foreach ($itemRecsList as $item)
		{
			$lineRec = getLineFieldValues($item);	
			if ($lineRec['orderLine'] == $prodLine)
			{
				unset($item->nsComplexObject_fields['itemName'] );
				unset($item->nsComplexObject_fields['onHand'] );
				unset($item->nsComplexObject_fields['quantityRemaining'] );				
				return $item;
			}
		}
	}
	else
	{
		$item = $itemList['item'];
		$lineRec = getLineFieldValues($item);	
		if ($lineRec['orderLine'] == $prodLine)
		{
			unset($item->nsComplexObject_fields['itemName'] );
			unset($item->nsComplexObject_fields['onHand'] );
			unset($item->nsComplexObject_fields['quantityRemaining'] );
			return $item;
		}
	}
	return null;
}

function convert2NSDate($inputDate)
{
	$intDate =  strtotime($inputDate, 0);;
	$outDate = date('c',$intDate);
	return $outDate;
}

function getShipMethod($scacCode)
{
	$shipMethodId = '';
	global $myNSclient;
	$lookupName = 'SCAC Code';
//	$custFldSrch - new nsComplexObject("CustomFieldList");

	$recTypeSrch = new nsComplexObject("RecordRef");
	$recTypeSrch->setFields(array("internalId" => "143"));

	$nameSrch = new nsComplexObject("SearchStringField");
	$nameSrch->setFields( array("searchValue" => $lookupName , "operator" => "is") );
	$lookupSearch = new nsComplexObject("CustomRecordSearchBasic");
	$lookupSearch->setFields(array("name" => $nameSrch,  "recType" => $recTypeSrch));

	// Invoke search operation
	$searchResponse = $myNSclient->search($lookupSearch);
	if (!$searchResponse->isSuccess)
	{
		return '';
	}
	$recPos = 0;

	foreach ($searchResponse->recordList as $rec)
	{
		$recFields = $rec->nsComplexObject_fields;
		$custFields = $recFields["customFieldList"];
		$custFieldList = $custFields->nsComplexObject_fields["customField"];
		$value = "";
		$field = "";
		foreach ($custFieldList as $custFieldRec)
		{
			$fieldName = $custFieldRec->nsComplexObject_fields["internalId"];
			if ($custFieldRec->nsComplexObject_type == 'StringCustomFieldRef' && $fieldName == "custrecord_ship_int_lookup_value")
			{
				$field = $custFieldRec->nsComplexObject_fields["value"];
			}
			else if ($custFieldRec->nsComplexObject_type == 'StringCustomFieldRef' && $fieldName == "custrecord_ship_int_lookup_field")
			{
				$value = $custFieldRec->nsComplexObject_fields["value"];
			}
		}
		if ($field == $scacCode )
		{
			return $value;
			break;
		}
	}

	return '';

}
function getLocationId($locName)
{
	global $myNSclient;
	
	$transactionSearch = new nsComplexObject("LocationSearchBasic");
	$transactionSearch->setFields(array(
				"nameNoHierarchy" => array("searchValue" => $locName, "operator" => "is")
				));
	
	// Invoke search operation
	$searchResponse = $myNSclient->search($transactionSearch);
	
	if (!$searchResponse->isSuccess)
	{
		Util::logError("Error searching for Pro Log ware house in Netsuite, " . $searchResponse->statusDetail[0]->message  );
		return null;
	}
	if ($searchResponse->totalRecords > 0)
	{
		$searchResponse->recordList->record->internalId;
		return true;
	}
	return null;
}
function checkShipmentInNS($proLogShipId)
{
	global $myNSclient;
	
	$customFieldList = new nsComplexObject("SearchCustomFieldList");
	$customField = new nsComplexObject("SearchStringCustomField");
	$customField->setFields(array("internalId" => "custbody_prolog_ship_id", "searchValue" => $proLogShipId, "operator" => "is"));
	$customFieldList->setFields(array ("customField" => array($customField)));
	$transactionSearch = new nsComplexObject("TransactionSearchBasic");
	$transactionSearch->setFields(array(
				"type" => array("searchValue"	=> array('_itemFulfillment'), "operator" => "anyOf")
				, "customFieldList" => $customFieldList
				));
	
	// Invoke search operation
	$searchResponse = $myNSclient->search($transactionSearch);
	
	if (!$searchResponse->isSuccess)
	{
		Util::logError("Error searching for ProLog Shipment id in Netsuite, " . $searchResponse->statusDetail[0]->message  );
		return false;
	}
	if ($searchResponse->totalRecords > 0)
	{
		return true;
	}
	return false;
}
function getInvoiceCashSale($orderId)
{
	global $myNSclient;
	try
	{
		$initRef = new nsInitializeRef(array("type" => "salesOrder", "internalId" => $orderId ) );
		try
		{
			$initRec = new nsInitializeRecord(array("type" => "cashSale", "reference" => $initRef ) );		
			$initResp = $myNSclient->initialize($initRec);
		}
		catch (Exception $e) {
			$initRec = new nsInitializeRecord(array("type" => "invoice", "reference" => $initRef ) );		
			$initResp = $myNSclient->initialize($initRec);
		}
		if (!$initResp->isSuccess)
		{
			Util::logError("Error creating billing record in Netsuite , " . $initResp->statusDetail[0]->message  );
			return null;
		}
		$invRec = $initResp->record;
		
		return $invRec;		
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return null;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return null;
	}
}
function getItemFulfillment($orderId)
{
	global $myNSclient;
	try
	{
		$initRef = new nsInitializeRef(array("type" => "salesOrder", "internalId" => $orderId ) );
		$initRec = new nsInitializeRecord(array("type" => "itemFulfillment", "reference" => $initRef ) );		
		$initResp = $myNSclient->initialize($initRec);
		if (!$initResp->isSuccess)
		{
			Util::logError("Error creating fulfillment in Netsuite , " . $initResp->statusDetail[0]->message  );
			return null;
		}
		$itemFulFillRec = $initResp->record;
		
		return $itemFulFillRec;
	}
	catch (SoapFault $soapFault)
	{
		Util::logError("SOAP Fault :" . $soapFault->faultstring);
		return null;
	}
	catch (Exception $e) {
		Util::logError($e->getMessage());
		return null;
	}
}

function getFieldValues($rec)
{
	global $lineValuesList,$itemList;
	$transactionFieldsListArray = array();
	$lineValuesList = array();
	$recFields = $rec->nsComplexObject_fields;
	foreach ($recFields  as  $fieldName => $fieldValue)
	{
		if ( $fieldValue instanceof nsComplexObject )
		{
			if ($fieldValue->nsComplexObject_type == 'RecordRef')
			{
				if (isReferenceObject($fieldName))
				{
					$fieldValue = $fieldValue->nsComplexObject_fields['internalId'];
				}
				else
				{
					$fieldValue = $fieldValue->nsComplexObject_fields['name'];
				}							
				$transactionFieldsListArray[$fieldName] = $fieldValue;
			}
			else if ($fieldValue->nsComplexObject_type == 'ShipAddress')
			{
				$shipFieldList = $fieldValue->nsComplexObject_fields;
				foreach ($shipFieldList as $shipField => $shipValue)
				{
					$transactionFieldsListArray[$shipField] = $shipValue;
				}
			}
			else if ($fieldValue->nsComplexObject_type == 'BillAddress')
			{
				$billFieldList = $fieldValue->nsComplexObject_fields;
				foreach ($billFieldList as $billField => $billValue)
				{
					$transactionFieldsListArray[$billField] = $billValue;
				}
			}
			else if ($fieldValue->nsComplexObject_type == 'CustomFieldList')
			{
				$custFieldList = $fieldValue->nsComplexObject_fields;
				$customFields = $custFieldList['customField'];
				if (array_key_exists(0,$customFields))
				{
					foreach ($customFields as $fieldsListArrayItem)
					{
						if ($fieldsListArrayItem->nsComplexObject_type == 'StringCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
										
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'BooleanCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
																
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'LongCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'DoubleCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'DateCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'SelectCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$customFieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
							
							if ( $customFieldValue instanceof nsComplexObject )
							{
								if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
								{
									$fieldValue = $customFieldValue->nsComplexObject_fields['internalId'];
								}
							}
						}
						$transactionFieldsListArray[$fieldName] = $fieldValue;
					}
				}
				else
				{
					if ($customFields->nsComplexObject_type == 'StringCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
									
					}
					else if ($customFields->nsComplexObject_type == 'BooleanCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
															
					}
					else if ($customFields->nsComplexObject_type == 'LongCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
					}
					else if ($customFields->nsComplexObject_type == 'DoubleCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
					}
					else if ($customFields->nsComplexObject_type == 'DateCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
					}
					else if ($customFields->nsComplexObject_type == 'SelectCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$customFieldValue = $customFields->nsComplexObject_fields['value'];
						
						if ( $customFieldValue instanceof nsComplexObject )
						{
							if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
							{
								$fieldValue = $customFieldValue->nsComplexObject_fields['internalId'];
							}
						}
					}
					$transactionFieldsListArray[$fieldName] = $fieldValue;
				}
			}
			else if (($fieldName == 'itemList'))
			{
				$itemList = $fieldValue->nsComplexObject_fields;
				$itemRecsList = $itemList['item'];
				if (array_key_exists(0,$itemRecsList))
				{					
					foreach ($itemRecsList as $item)
					{
						$lineValues = getLineFieldValues($item);		
						array_push($lineValuesList, $lineValues);			
					}
				}
				else
				{
					$item = $itemList['item'];
					$lineValues = getLineFieldValues($item);		
					array_push($lineValuesList, $lineValues);			
				}
			}
		}
		else
		{
			$transactionFieldsListArray[$fieldName] = $fieldValue;					
		}
	}
	return $transactionFieldsListArray;
}

function getLineFieldValues($item)
{
	$transactionLineFieldsListArray = array();
	$recFields = $item->nsComplexObject_fields;
	foreach ($recFields  as  $fieldName => $fieldValue)
	{
		if ( $fieldValue instanceof nsComplexObject )
		{
			if ($fieldValue->nsComplexObject_type == 'RecordRef')
			{
				if (isReferenceObjectLine($fieldName))
				{
					$fieldValue = $fieldValue->nsComplexObject_fields['internalId'];
				}
				else
				{
					$fieldValue = $fieldValue->nsComplexObject_fields['name'];
				}
				$transactionLineFieldsListArray[$fieldName] = $fieldValue;
			}
			else if ($fieldValue->nsComplexObject_type == 'CustomFieldList')
			{
				$custFieldList = $fieldValue->nsComplexObject_fields;
				$customFields = $custFieldList['customField'];
				if (array_key_exists(0,$customFields))
				{					
					foreach ($customFields as $fieldsListArrayItem)
					{
						if ($fieldsListArrayItem->nsComplexObject_type == 'StringCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
										
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'BooleanCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
																
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'LongCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'DoubleCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'DateCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$fieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
						}
						else if ($fieldsListArrayItem->nsComplexObject_type == 'SelectCustomFieldRef')
						{
							$fieldName = $fieldsListArrayItem->nsComplexObject_fields['internalId'];
							$customFieldValue = $fieldsListArrayItem->nsComplexObject_fields['value'];
							
							if ( $customFieldValue instanceof nsComplexObject )
							{
								if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
								{
									$fieldValue = $customFieldValue->nsComplexObject_fields['internalId'];
								}
							}
						}
						$transactionLineFieldsListArray[$fieldName] = $fieldValue;
					}
				}
				else
				{
					if ($customFields->nsComplexObject_type == 'StringCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
									
					}
					else if ($customFields->nsComplexObject_type == 'BooleanCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
															
					}
					else if ($customFields->nsComplexObject_type == 'LongCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
					}
					else if ($customFields->nsComplexObject_type == 'DoubleCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
					}
					else if ($customFields->nsComplexObject_type == 'DateCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$fieldValue = $customFields->nsComplexObject_fields['value'];
					}
					else if ($customFields->nsComplexObject_type == 'SelectCustomFieldRef')
					{
						$fieldName = $customFields->nsComplexObject_fields['internalId'];
						$customFieldValue = $customFields->nsComplexObject_fields['value'];
						
						if ( $customFieldValue instanceof nsComplexObject )
						{
							if ( $customFieldValue->nsComplexObject_type == 'ListOrRecordRef')
							{
								$fieldValue = $customFieldValue->nsComplexObject_fields['internalId'];
							}
						}
					}
					$transactionLineFieldsListArray[$fieldName] = $fieldValue;
				}
			}
		}
		else
		{
			$transactionLineFieldsListArray[$fieldName] = $fieldValue;					
		}
	}	
	
	return $transactionLineFieldsListArray;
}

function isReferenceObject($fieldName)
{
	global $referenceFieldArray;
	foreach ($referenceFieldArray as $refFieldName)
	{
		if ($fieldName == $refFieldName)
		{
			return true;
		}
	}
	return false;
}

function isReferenceObjectLine($fieldName)
{
	global $referenceFieldArrayLine;
	foreach ($referenceFieldArrayLine as $refFieldName)
	{
		if ($fieldName == $refFieldName)
		{
			return true;
		}
	}
	return false;
}

function defineFieldsListArray()
{
	global $referenceFieldArrayLine,$referenceFieldArray;
	
	$referenceFieldArray = array("entity");
	

	$referenceFieldArrayLine = array("");
	
	
}


function appendFieldValues(&$allTransactionFieldsValues)
{
	global $transactionFieldsListArray;

	foreach($transactionFieldsListArray as $fieldName => $fieldValue)
	{
		if ($allTransactionFieldsValues == '')
		{
			$allTransactionFieldsValues = '"' . $fieldValue . '"' ;
		}
		else
		{
			$allTransactionFieldsValues = $allTransactionFieldsValues . ',' . '"' . $fieldValue . '"' ;
		}
		$transactionFieldsListArray[$fieldName] = '';
	}
}

function appendLineFieldValues(&$allTransactionLineFieldsValues)
{
	global $transactionLineFieldsListArray;

	foreach($transactionLineFieldsListArray as $fieldName => $fieldValue)
	{
		if ($allTransactionLineFieldsValues == '')
		{
			$allTransactionLineFieldsValues = '"' . $fieldValue . '"' ;
		}
		else
		{
			$allTransactionLineFieldsValues = $allTransactionLineFieldsValues . ',' . '"' . $fieldValue . '"' ;
		}
		$transactionLineFieldsListArray[$fieldName] = '';
	}
}

function writeToFile($lineStr,$datHandle)
{
	$lineStr = $lineStr . "\r\n";
	fwrite($datHandle, $lineStr);
}

function endProcess() {
	global $totRecs, $errRecs, $succRecs;
	Util::logMessage("Orders processed                : " . $totRecs);
	Util::logMessage("Shipments processed in Netsuite : " . $succRecs);
	Util::logMessage("Shipments failed to process in Netsuite : " . $errRecs);
	Util::logMessage("Malvern to Netsuite Shipment integration completed..");
	Util::close();
}

$totRecs = 0;
$errRecs = 0;
$succRecs = 0;
$configFile = $argv[1];
$logFileName = $argv[2];
$inboundFile = $argv[3];
startProcess($configFile,$logFileName );
endProcess();


?>
