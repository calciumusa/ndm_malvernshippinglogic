<?php
/*
 * Name 		: class_Util.php
 * Author 		: Gopi Bellam
 * Date 		: 5/30/08
 * Description	: Utility Class
 */

class Util {
	
	static public $logHandle;
	//static public $configArr;
	static public $loginSXML;
	static public $appDataSXML;
	
	/* Log File Methods BEGIN */
	public static function init($configFile,$inputLogFile) {
		if ($inputLogFile == '')
		{
			$logFile = '../log/NetSuite2Shipment-' .  date("YmdHis") . '.log';
		}
		else
		{
			$logFile = $inputLogFile;
		}
		self::$logHandle = fopen($logFile, 'w');
		self::logMessage("NetSuite to Shipment integration started ....");
		//self::loadConfig($configFile);
		
	}

	public static function close() {
		fclose(self::$logHandle);
	}
	
	public static function logMessage($lineStr) {
		fwrite(self::$logHandle,date('D M j G:i:s T Y') . "\t" . $lineStr .  "\r\n");
		echo $lineStr . "\r\n";
	}
	
	public static function logError($lineStr) {
		fwrite(self::$logHandle,date('D M j G:i:s T Y') . "\tError:\t" . $lineStr .  "\r\n");
		echo "\tError:\t" . $lineStr .  "\r\n";
	}
		
	/* Log File Methods END */
	
	private static function loadConfig($inputConfigFile) {
		
		if (! isset($inputConfigFile) )
			$configFile = 'cis_config.xml';
		else
			$configFile = $inputConfigFile; 
		
		if (!file_exists($configFile))
		{
			throw new Exception("Config File " . $configFile . "does not exist.");
		    return;
		}
		
		$configSXML = simplexml_load_file($configFile);
		self::$loginSXML = $configSXML->NetSuiteLoginParams;
		self::$appDataSXML = $configSXML->AppData;
/*		
		self::$configArr["userName"]  = '';
		self::$configArr["password"] = '';
		self::$configArr["account"] = '';
		self::$configArr["roleKey"] = '';
		self::$configArr["WSDLUrl"] = '';
		
//		$stxUserName = '';
//		$stxPassword = '';
//		$stxAccount = '';
		self::$configArr["timeZone"] = 'America/Los_Angeles';

		$dom = new DOMDocument();
	
		$dom->load($configFile);
		foreach ($dom->getElementsByTagname('LoginParams') as $loginParams)
		{
			 foreach (($loginParams->childNodes) as $e)
			 {
			 	if (is_a($e, 'DOMElement'))
			 	{
			 		if ($e->tagName == 'NSuserName')
			 		{
			 			self::$configArr["userName"] = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'NSpassword')
			 		{
			 			self::$configArr["password"] = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'NSaccount')
			 		{
			 			self::$configArr["account"] = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'NSroleKey')
			 		{
			 			self::$configArr["roleKey"] = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'STXuserName')
			 		{
			 			$stxUserName = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'STXpassword')
			 		{
			 			$stxPassword = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'STXaccount')
			 		{
			 			$stxAccount = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'TimeZone')
			 		{
			 			self::$configArr["timeZone"] = htmlspecialchars($e->textContent);
			 		}
			 		elseif ($e->tagName == 'NSWSDLUrl')
			 		{
			 			self::$configArr["WSDLUrl"] = htmlspecialchars($e->textContent);
			 		}
			 		
				}
			 }
		}
*/
		
		
	}
	
	/* Other Utils */
	
	public static function appendArray(  $arrOFarr ) {
		$appendedArr = array();
		foreach($arrOFarr as $arr) {
			foreach($arr as $item)
			$appendedArr[] = $item;
		}
		
		return $appendedArr;
	}

	
	
}



?>