#!/bin/sh

echo "Malvern Shipping Integrtion started from new server" | mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -s "Malvern Shipping Integration started from new server" -- rramaswamy@demandsolutionsgroup.com

INTERFACE_DIR=/var/www/html/mantels/
cd $INTERFACE_DIR/bin
SYSDATE=`date "+%m%d%y%H%M %m/%d/%y %H:%M:%S"`
SYSDATE=`echo $SYSDATE | cut -c-10`
LOG=$INTERFACE_DIR/log/processShippingExtract$SYSDATE.log
$INTERFACE_DIR/bin/processShippingExtractMalvern.sh >$LOG


