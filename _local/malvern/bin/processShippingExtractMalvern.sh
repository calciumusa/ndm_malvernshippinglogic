#!/bin/sh
###############################################################################
# Program:    processShippingExtractMalvern.sh
# Author:     Venkat Sankaran, DSG
# Date:       11/19/2007
# Desc:       Shell script to extract Shipping records for Malvern
#
# Usage:
#               processShippingExtractMalvern.sh
#
# Date          Who             Version         Desc
# ===========   ========        =========       ================================
#
#
###############################################################################


# Check if the script is already running.
if [ $TERM = "dumb" ]; then
        . $HOME/bin/masterInit.sh
else
        . ./masterInit.sh
fi


MAIL_TO=`cat $INTERFACE_DIR/mail_list.dat`
ERR_MAIL_TO=rajuramaswamy@gmail.com,venkatsankaran@gmail.com
pCount=`ps -ef|egrep "mdShippingExtract.php"|grep -v grep|wc -l`

echo " PHP count= ${pCount} "

while [ $pCount -gt 0 ]
do
        echo "Netsuite integration already running..."
	sleep 20
	pCount=`ps -ef|egrep "mdMalvernExtract.php"|grep -v grep|wc -l`

#        echo "Netsuite integration already running. We will try again soon .." | mail -s "Error running On Demand Shipping Integration script" $MAIL_TO 
        echo "Netsuite integration already running. We will try again soon .." | mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -s "Error running On Demand Shipping Integration script" $MAIL_TO 
        #exit 1
done

#mail -s "Mantels ShippingExtract-On Demand Request-Started" $ERR_MAIL_TO

echo "processShippingExtractMalvern.sh On Demand started at " `date`

SYSDATE=`date "+%m%d%y%H%M %m/%d/%y %H:%M:%S"`
SYSDATE=`echo $SYSDATE | cut -c-10`

LOGFILE=$INTERFACE_DIR/log/processMalvernExtract$SYSDATE.log
DATA_DIR=$INTERFACE_DIR/data

MALVERN_FILE=NetSuite2Malvern.txt
MALVERN_UPDATE_ERR_FILE=NetSuite2MalvernErrors.txt
MALVERN_ARCHIVE=$DATA_DIR/archive/NetSuite2Malvern


MAIL_TO=`cat $INTERFACE_DIR/mail_list.dat`
# MAIL_TO=`cat $INTERFACE_DIR/mail_raju.dat`
CONFIG_FILE=$INTERFACE_DIR/login_params.xml

cd $INTERFACE_DIR 

php $INTERFACE_DIR/php/mdMalvernExtract.php $CONFIG_FILE $LOGFILE $DATA_DIR

echo "Checking for error in the log file ..."
ERR=`cat $LOGFILE|grep -c  -i "Error"`
if [ $ERR != 0 ]; then
	echo "Errors found in the log file..."
	SUBJECT="Malvern Shipping Extract Error Log"
#	mail -s "Error running processMalvernExtract script" $ERR_MAIL_TO <$LOGFILE
#	mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -s "Error running processMalvernExtract script" $`ERR_MAIL_TO <$LOGFILE
       echo "Malvern Shipping Extract Error Log Attached" | mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -a $LOGFILE -s "$SUBJECT" -- $MAIL_TO
fi

echo "Checking for exception in the log file ..."
EXP=`cat $LOGFILE|grep -c -i "Exception"`
if [ $EXP != 0 ]; then
	echo "Exception found in the log file..."
#	mail -s "Exception found when running processMalvernExtract script" $ERR_MAIL_TO <$LOGFILE
#	exit 0
fi

cd $DATA_DIR

echo "Checking for update error in the log file ..."
ERR=`cat $MALVERN_UPDATE_ERR_FILE|grep -c  -i "Error"`
if [ $ERR != 0 ]; then
	echo "Errors found in the update error log file..."
	SUBJECT="Details of error occurred when updating Shipping Flag during Malvern File Extract"
       echo "Details of error occurred when updating Shipping Flag during Malvern File Extract" | mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -a $MALVERN_UPDATE_ERR_FILE -s "$SUBJECT" -- $MAIL_TO
fi


echo "Checking for Malvern file ..."
if [ -f $MALVERN_FILE ]; then
	echo "Malvern file available..."
	SUBJECT="Malvern shipping extract File"
#	uuencode $MALVERN_FILE $MALVERN_FILE|mail -s "$SUBJECT" $MAIL_TO
       echo "Malvern shipping extract File Attached" | mutt -e "unmy_hdr from; my_hdr From: apache@demandsolutionsgroup.com" -a $MALVERN_FILE -s "$SUBJECT" -- $MAIL_TO
	mv $MALVERN_FILE $MALVERN_ARCHIVE-$SYSDATE.txt
fi

