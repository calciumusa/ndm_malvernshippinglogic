#!/bin/ksh
#
# program:      masterInit.sh
# Author:       Venkat Sankaran
# Owner:
#
# Narrative:
#               Script to initialize environment variables
#
# Dependencies:
#
# Usage:
#               . masterInit.sh
#
# Date          Who             Version         Desc
# ===========   ========        =========       ================================

# ==============================================================================

INTERFACE_DIR=/var/www/html/mantels
export INTERFACE_DIR
