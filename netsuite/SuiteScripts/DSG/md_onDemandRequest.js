
function service(request, response)
{
    var requestType =  request.getMethod();
	nlapiLogExecution('Debug','Checking','Request method : ' + requestType);
	if (requestType == 'GET')
	{
		var form = nlapiCreateForm('Submit Shipping Integration Request');
		form.addSubmitButton('Submit');
		response.writePage(form);

	}
	else if (requestType == 'POST')
	{
		var script = '';

//		script = 'http://209.40.205.110/mantels/submitShipIntJob.php';
		script = 'http://173.45.250.63/html/mantels/submitShipIntJob.php';
		var a = new Array();
		a['User-Agent-x'] = 'SuiteScript-Call';
		var resp = nlapiRequestURL( script , null, a);
		var body = resp.getBody();
		var headers = resp.getAllHeaders();
		//nlapiLogExecution('Debug','Checking','Header : ' + headers + ', Body : ' + body );
		var html = '<html>';
		html += '   <body>';
		html += '       <script language="JavaScript">';
		//html += '           if (window.opener) {';
		html += '                   alert("Request has been submitted");';
		//html += '           }';
		html += '            location.href="https://system.netsuite.com/app/center/card.nl"';
		html += '       </script>';
		html += '   </body>';
		html += '</html>';

		response.write(html);
	}
}


function serviceMalvern(request, response)
{
    var requestType =  request.getMethod();
	nlapiLogExecution('Debug','Checking','Request method : ' + requestType);
	if (requestType == 'GET')
	{
		var form = nlapiCreateForm('Submit Shipping Integration Request');
		form.addSubmitButton('Submit');
		response.writePage(form);

	}
	else if (requestType == 'POST')
	{
		var script = '';

//		script = 'http://209.40.205.110/mantels/submitShipIntJobMalvern.php';
		script = 'http://173.45.250.63/html/mantels/submitShipIntJobMalvern.php';
		var a = new Array();
		a['User-Agent-x'] = 'SuiteScript-Call';
		var resp = nlapiRequestURL( script , null, a);
		var body = resp.getBody();
		var headers = resp.getAllHeaders();
		//nlapiLogExecution('Debug','Checking','Header : ' + headers + ', Body : ' + body );
		var html = '<html>';
		html += '   <body>';
		html += '       <script language="JavaScript">';
		//html += '           if (window.opener) {';
		html += '                   alert("Request has been submitted");';
		//html += '           }';
		html += '            location.href="https://system.netsuite.com/app/center/card.nl"';
		html += '       </script>';
		html += '   </body>';
		html += '</html>';

		response.write(html);
	}


}

function serviceMalvernFulfill(request, response)
{
    var requestType =  request.getMethod();
	nlapiLogExecution('Debug','Checking','Request method : ' + requestType);
	if (requestType == 'GET')
	{
		var form = nlapiCreateForm('Submit Malvern Fulfillment Integration Request');
		form.addSubmitButton('Submit');
		response.writePage(form);

	}
	else if (requestType == 'POST')
	{
		var script = '';

//		script = 'http://209.40.205.110/mantels/submitFulfillIntJobMalvern.php';
		script = 'http://173.45.250.63/html/mantels/submitShipIntJobMalvern.php';
		var a = new Array();
		a['User-Agent-x'] = 'SuiteScript-Call';
		var resp = nlapiRequestURL( script , null, a);
		var body = resp.getBody();
		var headers = resp.getAllHeaders();
		//nlapiLogExecution('Debug','Checking','Header : ' + headers + ', Body : ' + body );
		var html = '<html>';
		html += '   <body>';
		html += '       <script language="JavaScript">';
		//html += '           if (window.opener) {';
		html += '                   alert("Request has been submitted");';
		//html += '           }';
		html += '            location.href="https://system.netsuite.com/app/center/card.nl"';
		html += '       </script>';
		html += '   </body>';
		html += '</html>';

		response.write(html);
	}


}
